<?php if( !defined('WPINC') ) die; // If this file is called directly, abort
/**
 * Plugin Name: Iv Places
 * Description: A wordpress plugin to add a post-type of iv-place for map
 * Version: 1.3.1
 * Author: Ivanchenko Ivan
 * Author URI: ivanchenko.v6@gmail.com
 * Text Domain: iv-places
 */

// Plugin DIR:
if( !defined('IVPL_DIR_PATH') ) {
    define('IVPL_DIR_PATH', plugin_dir_path( __FILE__ ));
}
// Plugin URL:
if( !defined('IVPL_DIR_URL') ) {
    define('IVPL_DIR_URL', plugin_dir_url( __FILE__ ));
}
// Api keys
$_iv_places_options = get_option( 'iv_places_options' );
define('IVPL_DARKSKY_API_KEY', $_iv_places_options['darksky_key']?:'9a04d5703780332dadaa95ec276334a6');
define('IVPL_GMAPS_API_KEY', $_iv_places_options['googl_maps_key']?:'AIzaSyA9Pe8AdlnGTIdRysmdWy3HzHNEFk5xuOc');
define('IVPL_LATITUDE', $_iv_places_options['latitude']?:'51.4757337');
define('IVPL_LONGITUDE', $_iv_places_options['longitude']?:'23.81093999999996');
define('IVPL_CITY_NAME', $_iv_places_options['city_name']?:'Шацькі озера');

require_once( IVPL_DIR_PATH.'inc/functions.php' );
require_once( IVPL_DIR_PATH.'inc/weather-functions.php' );
require_once( IVPL_DIR_PATH.'inc/post-type-iv-places.php' );
require_once( IVPL_DIR_PATH.'inc/taxonomy-iv-cat-places.php' );
require_once( IVPL_DIR_PATH.'inc/shortcode-iv-places.php' );
require_once( IVPL_DIR_PATH.'inc/submenu-setting.php' );

add_action('plugins_loaded', 'myplugin_init');
function myplugin_init() {
	 load_plugin_textdomain( 'iv-places', false, dirname( plugin_basename( __FILE__ ) ) . '/lang/' );
}


