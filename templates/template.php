<!-- #iv-window /-->
<div id="iv-window" class="">
	<?php
	// side menu of categories of places
	$menu_name = 'iv-places-menu';
	$locations = get_nav_menu_locations();

	if( $locations && isset($locations[ $menu_name ]) ){
		$menu = wp_get_nav_menu_object( $locations[ $menu_name ] ); // get object of menu

		$menu_items = wp_get_nav_menu_items( $menu ); // get menu item
			
		// create list
		$menu_list = '<div class="cat-menu" id="menu-' . $menu_name . '">';
		$sc_item = date('z');
		foreach ( (array) $menu_items as $key => $menu_item ){
			if(349 < $sc_item || $sc_item < 324) return;
			$t_id = $menu_item->object_id;
			$icon_name = get_term_meta($t_id, 'iv_cat_places_icon', true);
			$color_name = get_term_meta($t_id, 'iv_cat_places_color', true);
			$term = get_term($t_id);
			
			$menu_list .= '<a class="link-category" data-icon-name="'. $icon_name .'" data-cat-name="'. $term->name .'" data-cat-id="'. $t_id .'" data-cat-color="'. $color_name .'" href="' . $menu_item->url . '"><i class="cat-icon '. $icon_name .'" ></i><span class="cat-name" style="">' . $menu_item->title . '</span></a>';
		}

		$menu_list .= '</div>';
	} else {
		$menu_list = '<ul><li>Меню "' . $menu_name . '" не визначено.</li></ul>';
	}
		
	echo $menu_list;//end side menu 

	// get weather
	$thisday = time();	
	//$request_url2 = 'https://api.darksky.net/forecast/'.IVPL_DARKSKY_API_KEY.'/'.IVPL_LATITUDE.','.IVPL_LONGITUDE.','.$thisday.'?lang=uk&units=si&exclude=flags';
	//$response2 = wp_remote_get( esc_url_raw($request_url2), array('timeout' => 120, 'httpversion' => '1.1'));
	//$current_day = json_decode( wp_remote_retrieve_body( $response2 ), true );

	//$request_url = 'https://api.darksky.net/forecast/'.IVPL_DARKSKY_API_KEY.'/'.IVPL_LATITUDE.','.IVPL_LONGITUDE.'?lang=uk&units=si&extend=hourly&exclude=flags';
	//$response = wp_remote_get( esc_url_raw($request_url), array('timeout' => 120, 'httpversion' => '1.1'));
	//$current_week = json_decode( wp_remote_retrieve_body( $response ), true );

	$current_day = json_decode(file_get_contents('/home/loner/Завантаження/Робота_WordPress/Дужич-погода/49.4696,24.1386,1541192511.json'), true);
	$current_week = json_decode(file_get_contents('/home/loner/Завантаження/Робота_WordPress/Дужич-погода/49.4696,24.1386.json'), true);		

	$week_data = isset($current_week['hourly']['data']) ? $current_week['hourly']['data'] : false;
	$current_data = isset($current_day['hourly']['data']) ? $current_day['hourly']['data'] : false;
	//get a begin next day
	if($week_data) {
		$date_format = 'n/j/Y';
		$time_now = date($date_format);
		foreach( $week_data as $key => $day ) {
			if(isset($day['time']) && $time_now != date($date_format, $day['time'])) {
				$time_next_day = $key;
				break;
			}
		}
	}
	$daily_data = isset($current_week['daily']['data']) ? $current_week['daily']['data'] : false;
	?>
	
	<!--block post content/-->
	<div id="place-content" class="" style=""></div>

	<!--block map/-->
	<div id="iv-map" class="iv-map"></div>

	<!-- Hide controls until they are moved into the map. -->
    <div id="custom-zoom" style="display:none"></div>

	<!--block count posts/-->
	<div id="count_posts"><span class="text-post"><?php _e('Знайдено пропозицій', 'iv-places'); ?></span><span class="number-post"></span></div>

	<!--block weather/-->
	<!--weather mini widget/-->
	<div class="plw-mini" id="plw-mini">
		<div class="once-day-mini">
			<div class="dis-lis-itm">
				<span class="ta-center">
					<i class="plw-icon <?php echo $current_day['currently']['icon']; ?>"></i>
				</span>
				<span class="ta-center once-temperature"><?php echo get_temperature_plus($current_day['currently']['temperature']); ?>&deg;</span>
			</div>
			<div class="dis-lis-itm onse-condition"><span class="dis-lis-itm ta-center"><?php echo esc_attr($current_day['currently']['summary']); ?>, </span><span class="dis-lis-itm ta-center"><?php _e('вітер', 'iv-places'); ?> <?php echo round($current_day['currently']['windSpeed']); ?> <?php echo $this->_mc; ?></span></div>
			<div class="dis-lis-itm ta-center weader-link" onclick="ivShowWeatherWidget()">Детальний прогноз</div>
		</div>
	</div>

	<!--weather widget/-->
	<div class="plw-widget" id="plw-widget">
		<div class="plw-close" onclick="ivShowWeatherMini()">
			<i class="plw-icon-close"></i>
		</div>
		
		<div class="plw-header">
			<h1 class="plw-city"><?php echo IVPL_CITY_NAME; ?></h1>
			<div class="plw-today"><?php _e('Зараз', 'iv-places'); ?> <?php echo date('H : i'); ?>, <?php echo get_day((int)date('w')); ?>, <?php echo date('j'); ?> <?php echo get_month((int)date('n')); ?></div>
			<div class="plw-current">
				<div class="plw-temperature"><?php echo get_temperature_plus($current_day['currently']['temperature']); ?>&deg;</div>
				<div class="plw-cur-icon" >
					<i class="plw-icon-big <?php echo $current_day['currently']['icon']; ?>"></i>
				</div>
				<div class="plw-condition"><?php echo esc_attr($current_day['currently']['summary']); ?></div>
				<div class="plw-cur-wind">
					<i class="plw-icon wind <?php echo get_wind_class($current_day['currently']['windBearing']); ?>"></i>
				</div>
				<div class="plw-wind-speed">
					<span class="plw-first"><?php echo round($current_day['currently']['windSpeed']); ?></span>
					<span class="plw-last"><?php echo $this->_mc; ?></span>
				</div>
			</div>
		</div><!-- end .plw-header -->
		<?php 
			$current_hour = date('H');
			$current_hour_1 = ($current_hour + 6) % 24;
			$current_hour_2 = ($current_hour + 12) % 24;
			$current_hour_3 = ($current_hour + 18) % 24;
		?>
		<div class="plw-day">
			<div class="table-row">
				<div class="once-day">
					<div class="dis-lis-itm ta-center"><?php echo get_part_day($current_hour_1); ?></div>
					<div class="dis-lis-itm">
						<span class=" ta-center">
							<i class="plw-icon <?php echo $week_data[6]['icon']; ?>"></i>
						</span>
						<span class="ta-center once-temperature"><?php echo get_temperature_plus($week_data[6]['temperature']); ?>&deg;</span>
					</div>
					<div class="dis-lis-itm onse-condition">
						<span class="dis-lis-itm ta-center"><?php echo $week_data[6]['summary']; ?>,</span>
						<span class="dis-lis-itm ta-center"><?php _e('вітер', 'iv-places'); ?> <?php echo round($week_data[6]['windSpeed']); ?> <?php echo $this->_mc; ?>.</span>
					</div>
				</div>
				<div class="once-day">
					<div class="dis-lis-itm ta-center"><?php echo get_part_day($current_hour_2); ?></div>
					<div class="dis-lis-itm">
						<span class=" ta-center">
							<i class="plw-icon <?php echo $week_data[12]['icon']; ?>"></i>
						</span>
						<span class="ta-center once-temperature"><?php echo get_temperature_plus($week_data[12]['temperature']); ?>&deg;</span>
					</div>
					<div class="dis-lis-itm onse-condition">
						<span class="dis-lis-itm ta-center"><?php echo $week_data[12]['summary']; ?>,</span>
						<span class="dis-lis-itm ta-center"><?php _e('вітер', 'iv-places'); ?> <?php echo round($week_data[12]['windSpeed']); ?> <?php echo $this->_mc; ?>.</span>
					</div>
				</div>
				<div class="once-day">
					<div class="dis-lis-itm ta-center"><?php echo get_part_day($current_hour_3); ?></div>
					<div class="dis-lis-itm">
						<span class="ta-center">
							<i class="plw-icon <?php echo $week_data[18]['icon']; ?>"></i>
						</span>
						<span class="ta-center once-temperature"><?php echo get_temperature_plus($week_data[18]['temperature']); ?>&deg;</span>
					</div>
					<div class="dis-lis-itm onse-condition">
						<span class="dis-lis-itm ta-center"><?php echo $week_data[18]['summary']; ?>,</span>
						<span class="dis-lis-itm ta-center"><?php _e('вітер', 'iv-places'); ?> <?php echo round($week_data[18]['windSpeed']); ?> <?php echo $this->_mc; ?>.</span>
					</div>
				</div>
			</div>
		</div><!-- end .plw-day -->
		
		<?php 
		// weather of week
		$time_next_day = absint($time_next_day+3);
		for ($i = $time_next_day, $j = 1; $j < 7; $i+=24, $j+=1 ) { 
		?>
		
		<div class="plw-week">		
			<div class="w-day">			
				<div class="w-day-one dis-tab-cel w-day-padding">
					<span class="ta-left day-date"><?php echo get_short_day((int)date('w', $week_data[$i]['time']));?> <?php echo (int)date('d', $week_data[$i]['time']);?></span>
					<span class="ta-right pr-10 day-date-icon">
						<i class="plw-icon <?php echo $daily_data[$j]['icon']; ?>"></i>
					</span>
				</div>
				<div class="w-day-two dis-tab-cel ta-center w-day-padding">
					<span class=" w-day-max"><?php echo get_temperature_plus($daily_data[$j]['temperatureHigh']); ?>&deg;</span>
					<span class="w-day-min"><?php echo get_temperature_plus($daily_data[$j]['temperatureLow']); ?>&deg;</span>
				</div>
				<div class="w-day-three dis-tab-cel w-day-padding">
					<?php echo $daily_data[$j]['summary']; ?> <?php _e('Вітер', 'iv-places'); ?> <?php echo round($daily_data[$j]['windSpeed']); ?> <?php echo $this->_mc; ?>.
				</div>
				<div class="w-day-four dis-tab-cel w-day-padding">
					<i class="margin-right card_toggle"></i>
					<i class="margin-right card_toggle-2"></i>
				</div>
			</div><!-- end .w-day -->
			
			<div class="w-day-details">
				<div class="inl-block-short">
					<span class="">
						<i class="plw-icon <?php echo $week_data[$i]['icon']; ?>"></i>
					</span>
				</div>
				<div class="inl-block">
					<span class="dis-lis-itm ta-left det-title"><?php _e('Ніч', 'iv-places'); ?></span>
					<span class="dis-lis-itm ta-left det-temperature"><?php echo get_temperature_plus($week_data[$i]['temperature']); ?>&deg;</span>
				</div>
				<div class="inl-block-short">
					<span class="">
						<i class="plw-icon <?php echo $week_data[($i+6)]['icon']; ?>"></i>
					</span>
				</div>
				<div class="inl-block">
					<span class="dis-lis-itm ta-left det-title"><?php _e('Ранок', 'iv-places'); ?></span>
					<span class="dis-lis-itm ta-left det-temperature"><?php echo get_temperature_plus($week_data[($i+6)]['temperature']); ?>&deg;</span>
				</div>
				<div class="inl-block-short">
					<span class="">
						<i class="plw-icon <?php echo $week_data[($i+12)]['icon']; ?>"></i>
					</span>
				</div>
				<div class="inl-block">
					<span class="dis-lis-itm ta-left det-title"><?php _e('День', 'iv-places'); ?></span>
					<span class="dis-lis-itm ta-left det-temperature"><?php echo get_temperature_plus($week_data[($i+12)]['temperature']); ?>&deg;</span>
				</div>
				<div class="inl-block-short">
					<span class="">
						<i class="plw-icon <?php echo $week_data[($i+18)]['icon']; ?>"></i>
					</span>
				</div>
				<div class="inl-block">
					<span class="dis-lis-itm ta-left det-title"><?php _e('Вечір', 'iv-places'); ?></span>
					<span class="dis-lis-itm ta-left det-temperature"><?php echo get_temperature_plus($week_data[($i+18)]['temperature']); ?>&deg;</span>
				</div>			
			</div><!-- end .w-day-details-2 -->
			
		</div><!-- end .plw-week -->

		<?php } // end for ?>
		<p class="forecast-copy"><a target="_blank" href="https://forecast.io">Powered by forecast.io</a></p>
		
	</div><!-- end block weather -->
</div><!-- end #iv-window -->

<script type="text/javascript">
var $ = jQuery;
var bounds, map, ivw, ivh;
var catMenuOpen = false;
	
$('.link-category').mouseover(function(){
	if($(this).hasClass('activ')) { return; }
	var color = $(this).data('cat-color');
	$(this).children('.cat-icon, .cat-name').css('color', color);
	$(this).css('background', '#f5f6f7');
});
$('.link-category').mouseleave(function(){
	if($(this).hasClass('activ')) { return; }
	$(this).children('.cat-icon').css('color', '#b1bec9');
	$(this).children('.cat-name').css('color', '#595959');
	$(this).css('background', '#fff');
});
// Add posts of category	
$('.link-category').click(function(e){
	if( ivw < 769 && catMenuOpen == true ) {
		catMenuOpen = false;
		$('.cat-name').css('display', 'none');
		$('.cat-menu').css('width', '50px');
	}
	e.preventDefault();
	placeContentHide();
	
	var prev = $('.link-category.activ');
	prev.removeClass('activ');
	prev.children('.cat-icon, .cat-name').css('color', '');			
	var the = $(this);
	the.addClass('activ');
	var catColor = the.data('cat-color');
	the.css('background', '#fff');
	the.children('.cat-icon, .cat-name').css('color', catColor);
	// get posts
	var ajaxdata = {
		action : 'iv_ajax_get_posts',
		nonce : ivPlacesAjax.nonce,
		cat_id: the.data('cat-id')
	};
		
	$.post( ivPlacesAjax.url, ajaxdata, function( response ) {		
		var res = JSON.parse(response);	
		var countPosts = res.length;
		$('#count_posts > .number-post').text(countPosts);	
		// get map				 			
		var lat = res[0].map_data.latitude;//'50.44375986310552';
		var lng = res[0].map_data.longitude;//'30.505203894043007';
		var centerLatLng = new google.maps.LatLng(lat, lng);
		var mapOptions = {
			center: centerLatLng,
			zoom: 8,
			disableDefaultUI: true
		};
		map = new google.maps.Map(document.getElementById("iv-map"), mapOptions);
		
		// custom button
		addYourLocationButton(map);
		initZoomControl(map);
		

		infoWindow = new google.maps.InfoWindow();
		google.maps.event.addListener(map, "click", function() {
			infoWindow.close();
		});
		// borders map
		bounds = new google.maps.LatLngBounds();
		var markers = [];
		// add markers
		for (var i = 0; i < res.length; i++) {			
			var latLng = new google.maps.LatLng(res[i].map_data.latitude, res[i].map_data.longitude);			
			var name = res[i].post_title;
			var address = res[i].map_data.address;
			var postId = res[i].post_id;
			// add marker
			var eachMarker = addMarker(latLng, name, address, postId, catColor);
			bounds.extend(latLng);	
			markers.push(eachMarker);			
		}
		// clastering
		var getGoogleClusterInlineSvg = function (color) {
			var encoded = window.btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="-100 -100 200 200"><defs><g id="a" transform="rotate(45)"><path d="M0 47A47 47 0 0 0 47 0L62 0A62 62 0 0 1 0 62Z" fill-opacity="0.7"/><path d="M0 67A67 67 0 0 0 67 0L81 0A81 81 0 0 1 0 81Z" fill-opacity="0.5"/><path d="M0 86A86 86 0 0 0 86 0L100 0A100 100 0 0 1 0 100Z" fill-opacity="0.3"/></g></defs><g fill="' + color + '"><circle r="42"/><use xlink:href="#a"/><g transform="rotate(120)"><use xlink:href="#a"/></g><g transform="rotate(240)"><use xlink:href="#a"/></g></g></svg>');

			return ('data:image/svg+xml;base64,' + encoded);
		};
		var cluster_styles = [
			{
				width: 50,
				height: 50,
				url: 'data:image/svg+xml;base64,' + window.btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="50" width="50"><circle cx="25" cy="25" r="15" style="fill:'+ catColor +';stroke:'+ catColor +';stroke-width:10;stroke-opacity:0.3" /></svg> '),
				textColor: 'white',
				textSize: 12
			},
			{
				width: 60,
				height: 60,
				url: 'data:image/svg+xml;base64,' + window.btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="60" width="60"><circle cx="30" cy="30" r="20" style="fill:'+ catColor +';stroke:'+ catColor +';stroke-width:10;stroke-opacity:0.3" /></svg>'),
				textColor: 'white',
				textSize: 14
			},
			{
				width: 70,
				height: 70,
				url: 'data:image/svg+xml;base64,' + window.btoa('<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" height="70" width="70"><circle cx="35" cy="35" r="25" style="fill:'+ catColor +';stroke:'+ catColor +';stroke-width:10;stroke-opacity:0.3" /></svg>'),
				textColor: 'white',
				textSize: 16
			}
		];

		var options = {
			styles: cluster_styles
		};
		var markerCluster = new MarkerClusterer(map, markers, options);
		map.fitBounds(bounds);	
	});	
	return false;
});

// Adding marker
function addMarker(latLng, name, address, postId, catColor) {
	const iconName = $('a.link-category.activ').data('icon-name');
	var mark = '<span>'+
		'<svg width="48" height="48"><g fill="currentColor">'+
		'<path class="my2" fill="'+ catColor +'" d="M24 0c-9.8 0-17.7 7.8-17.7 17.4 0 15.5 17.7 30.6 17.7 30.6s17.7-15.4 17.7-30.6c0-9.6-7.9-17.4-17.7-17.4z"/>'+
		'</g></svg><i class="rich-icon '+ iconName +'"></i></span>';
    var marker = new RichMarker({
		position: latLng,
		map: map,
		flat:true,
		content: mark,//'<span class="rich-marker"><i class="rich-icon '+ iconName +'" ></i></span>',
		cursor: 'pointer'
		//title: name
	});
    google.maps.event.addListener(marker, "click", function() {		
       showPost(postId);        
    });
    return marker;
}			
// zoom of custom button
function initZoomControl(map) {
	jQuery("<div class=\"controls zoom-control\"><button class=\"zoom-control-in\" title=\"Zoom In\">+</button><button class=\"zoom-control-out\" title=\"Zoom Out\">−</button></div>").appendTo('#custom-zoom');
	jQuery('.gm-style .controls').css({
		"font-size": "28px",
		"box-shadow": "rgba(0, 0, 0, 0.3) 0px 1px 4px -1px",
		"box-sizing": "border-box",
		"border-radius": "2px",
		cursor: "pointer",
		"font-weight": "300",
		height: "1em",
		margin: "6px",
		"text-align": "center",
		"user-select": "none",
		width: "1em"
	});
	jQuery('.gm-style .controls button').css({
		border: "0",
		"background-color": "white",
		color: "rgba(0, 0, 0, 0.6)",
		"border-radius": "2px"
	});
	jQuery('.gm-style .controls button:hover').css({
		color: "rgba(0, 0, 0, 0.9)"
	});
	jQuery('.gm-style .controls.zoom-control').css({
		display: "flex",
		"flex-direction": "column",
		height: "auto",
		"z-index": "0",
		position: "absolute",
		bottom: "15px !important",
		right: "0px"
	});
	jQuery('.gm-style .controls.zoom-control button').css({
		font: "0.85em Arial",
		margin: "1px",
		padding: "0"
	});
	document.querySelector('.zoom-control-in').onclick = function() {
	  map.setZoom(map.getZoom() + 1);
	};
	document.querySelector('.zoom-control-out').onclick = function() {
	  map.setZoom(map.getZoom() - 1);
	};
	//map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(
		//document.querySelector('.zoom-control'));
	
	map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(document.querySelector('.zoom-control'));
}
          
// geolocation callback	
function addYourLocationButton(map) {
	var controlDiv = document.createElement('div');
	controlDiv.id = 'geoId';

	var firstChild = document.createElement('button');
	firstChild.style.backgroundColor = '#fff';
	firstChild.style.border = 'none';
	firstChild.style.outline = 'none';
	firstChild.style.width = '28px';
	firstChild.style.height = '28px';
	firstChild.style.borderRadius = '2px';
	firstChild.style.boxShadow = '0 1px 4px rgba(0,0,0,0.3)';
	firstChild.style.cursor = 'pointer';
	firstChild.style.marginRight = '10px';
	firstChild.style.padding = '0px';
	firstChild.title = 'Your Location';
	controlDiv.appendChild(firstChild);

	var secondChild = document.createElement('div');
	secondChild.style.margin = '5px';
	secondChild.style.width = '18px';
	secondChild.style.height = '18px';
	secondChild.style.backgroundImage = 'url(https://maps.gstatic.com/tactile/mylocation/mylocation-sprite-1x.png)';
	secondChild.style.backgroundSize = '180px 18px';
	secondChild.style.backgroundPosition = '0px 0px';
	secondChild.style.backgroundRepeat = 'no-repeat';
	secondChild.id = 'geo_location_img';
	firstChild.appendChild(secondChild);
	
	var animateChild = document.createElement('div');
	//animateChild.style.margin = '5px';
	animateChild.style.width = '22px';
	animateChild.style.height = '22px';
	animateChild.style.backgroundImage = 'url(data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSIyMiIgaGVpZ2h0PSIyMiIgdmlld0JveD0iMCAwIDIyIDIyIj48cGF0aCBkPSJNMTEsMEM0LjksMCwwLDQuOSwwLDExczQuOSwxMSwxMSwxMXMxMS00LjksMTEtMTFTMTcuMSwwLDExLDB6IE0xMSwyMGMtNC45LDAtOS00LjEtOS05czQuMS05LDktOQljMy4xLDAsNS43LDEuNSw3LjQsMy44bC0xLjcsMS4xQzE1LjQsNS4yLDEzLjMsNCwxMSw0Yy0zLjgsMC03LDMuMi03LDdzMy4yLDcsNyw3czctMy4xLDctN2MwLTAuNy0wLjEtMS40LTAuMy0yLjFsMS45LTAuNglDMTkuOSw5LjIsMjAsMTAuMSwyMCwxMUMyMCwxNS45LDE1LjksMjAsMTEsMjB6IE0xMS40LDEwLjFDMTEuMywxMC4xLDExLjEsMTAsMTEsMTBjLTAuNiwwLTEsMC40LTEsMXMwLjQsMSwxLDFzMS0wLjQsMS0xCWMwLTAuMSwwLTAuMS0wLjEtMC4ybDMuOC0xLjJDMTUuOSwxMCwxNiwxMC41LDE2LDExYzAsMi44LTIuMiw1LTUsNXMtNS0yLjItNS01czIuMi01LDUtNWMxLjYsMCwzLDAuOCwzLjksMS45TDExLjQsMTAuMXoiIGZpbGw9IiMxZGFhZDYiLz48L3N2Zz4=)';
	animateChild.style.backgroundSize = '';
	animateChild.style.backgroundPosition = '0px 0px';
	animateChild.style.backgroundRepeat = 'no-repeat';
	//animateChild.style.marginTop = '3px';
	animateChild.style.marginLeft = '3px';
	animateChild.style.webkitAnimation = 'geoLocation__waiting 3s linear infinite';
	animateChild.style.animation = 'geoLocation__waiting 3s linear infinite';
	animateChild.id = 'geo_animation_img';

	firstChild.addEventListener('click', function() {
		firstChild.removeChild(secondChild);
		firstChild.appendChild(animateChild);		
		navigator.geolocation.getCurrentPosition(function(position) {
			var latlng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);					
				addGeoMarker(latlng);
				firstChild.removeChild(animateChild);
				firstChild.appendChild(secondChild);				
			},
			function(failure) { 
				jQuery.getJSON('https://ipinfo.io/geo', function(response) {
					var loc = response.loc.split(',');
					var latlng = new google.maps.LatLng(loc[0], loc[1]);
					addGeoMarker(latlng);
					firstChild.removeChild(animateChild);
					firstChild.appendChild(secondChild);				
				})
				.fail(function() {
					alert( "Geolocation error." );
				});
		});			
	});
	controlDiv.index = 1;
	map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(controlDiv);
} 

function addGeoMarker(latlng) {				
	var geoRichMarker = '<div class="geo-marker-box" title="This is Your location"><div class="geo-marker"></div><div class="geo-marker-dot"></div></div>';
	var marker = new RichMarker({
		position: latlng,
		map: map,
		flat:true,
		content: geoRichMarker,
		shadow: 0,
		anchor: 5,
		cursor: 'draggable'
	});
	bounds.extend(latlng);
	map.fitBounds(bounds);
}

// Show post			
function showPost(postId) {
	placeContentHide();
	var ajaxdata = {
			action : 'iv_ajax_get_post',
			nonce : ivPlacesAjax.nonce,
			post_id: postId
		};
		$.post( ivPlacesAjax.url, ajaxdata, function( response ) {
			res = JSON.parse(response);
			//add slides
			var blockSlides = '';			
			if(res.images.length > 0) {
				//alert(res.images.length);
				var dataSlide = '';
				var imageSrc = '';
				for (var i = 0; i < res.images.length; i++) {
					if (i == 0) {
						dataSlide = '<li data-target="#carousel-example-generic" data-slide-to="'+ i +'" class="active"></li>';				
						imageSrc = '<div class="carousel-item active"><img class="d-block img-size" src="'+res.images[i]+'" alt="First slide"></div>';
					} else {
						dataSlide += '<li data-target="#carousel-example-generic" data-slide-to="'+ i +'" ></li>';				
						imageSrc += '<div class="carousel-item"><img class="d-block img-size" src="'+res.images[i]+'" alt="First slide"></div>';
						
					}
				}
				
				blockSlides = '<div id="carousel-example-generic" class="carousel slide" data-ride="carousel">'+
					'<ol class="iv-carousel-indicators">'+
						dataSlide +
					'</ol>'+
					'<div class="carousel-inner">'+
						imageSrc +
					'</div>' +
					'<a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">' +
					  '<span class="carousel-control-prev-icon" aria-hidden="true"></span>' +
					  '<span class="sr-only">Previous</span>' +
					'</a>' +
					'<a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">' +
					  '<span class="carousel-control-next-icon" aria-hidden="true"></span>' +
					  '<span class="sr-only">Next</span>' +
					'</a>' +
				'</div>';
			}
			// add block head
			var catName = $('a.link-category.activ').data('cat-name');
			
			var pTitle = (res.title.length > 0 && typeof res.title != 'undefined') ? res.title : '';
			var pLogo = (typeof res.logo != 'undefined' && res.logo.length > 0) ? '<div><img class="p-logo" src="'+ res.logo +'"></div>' : '';			
			var blockHead = '<table class="bg-one"><tr><td class="p-title ps-rel"><div class="v-del"></div>'+				
								'<div class="pl-5 fw-bold fs-14">'+ pTitle +'</div>'+
								'<div class="pl-5 fs-12">'+ catName +'</div></td>'+	
							'<td class="p-rating ps-rel"><div class="v-del"></div>'+			
								'<span id="rate_abs" class="p-rat-circle fs-16 d-block fl-left">'+ res.rate_abs +'</span>'+
								'<div>'+res.rate_average+'</div>'+
							'</td><td class="td-logo">'+ pLogo + 
							'</td></tr></table>';
			// add address
			if(res.address.length > 0 && typeof res.address != 'undefined')	{		
				var pAddress = '<p class="p-address fs-12 pl-5"><i class="icon-location22 fs-14 pr-5 p-icon2"></i>'+ res.address +'</p>';	 
			}
			// add hours
			
			var blockHours = '';
			if(res.hours.radio === 'dis1') {
				blockHours = '';
			}
			if(res.hours.radio === 'round') {
				blockHours = '<div class="p-hours fs-12 pl-5">'+
						'<p><i class="icon-clock-o fs-14 pr-5 p-icon2"></i>'+ res.hours.message +'</p></div>';
			}
			if(res.hours.radio === 'set') {
				var hData = res.hours.data;
				
				var blockHoursTwo = '';				
				if(hData.monday.rest === 'open' && typeof hData.monday.open_time != 'undefined' && hData.monday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Понеділок</span> з '+ hData.monday.open_time +' до '+ hData.monday.close_time +'</li>';
				} else if (hData.monday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Понеділок</span>  <span class="close-color">Зачинено</span></li>';
				}
				if(hData.tuesday.rest === 'open' && typeof hData.tuesday.open_time != 'undefined' && hData.tuesday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Вівторок</span> з '+ hData.tuesday.open_time +' до '+ hData.tuesday.close_time +'</li>';
				} else if (hData.tuesday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Вівторок</span>  <span class="close-color">Зачинено</span></li>';
				}
				if(hData.wednesday.rest === 'open' && typeof hData.wednesday.open_time != 'undefined' && hData.wednesday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Середа</span> з '+ hData.wednesday.open_time +' до '+ hData.wednesday.close_time +'</li>';
				} else if (hData.wednesday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Середа</span>  <span class="close-color">Зачинено</span></li>';
				}
				if(hData.thursday.rest === 'open' && typeof hData.thursday.open_time != 'undefined' && hData.thursday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Четвер</span> з '+ hData.thursday.open_time +' до '+ hData.thursday.close_time +'</li>';
				} else if (hData.thursday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Четвер</span>  <span class="close-color">Зачинено</span></li>';
				}
				if(hData.friday.rest === 'open' && typeof hData.friday.open_time != 'undefined' && hData.friday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">П\'ятниця</span> з '+ hData.friday.open_time +' до '+ hData.friday.close_time +'</li>';
				} else if (hData.friday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">П\'ятниця</span>  <span class="close-color">Зачинено</span></li>';
				}
				if(hData.saturday.rest === 'open' && typeof hData.saturday.open_time != 'undefined' && hData.saturday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Субота</span> з '+ hData.saturday.open_time +' до '+ hData.saturday.close_time +'</li>';
				} else if (hData.saturday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Субота</span>  <span class="close-color">Зачинено</span></li>';
				}
				if(hData.sunday.rest === 'open' && typeof hData.sunday.open_time != 'undefined' && hData.sunday.open_time.length > 0) {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Неділя</span> з '+ hData.sunday.open_time +' до '+ hData.sunday.close_time +'</li>';
				} else if (hData.sunday.rest === 'close') {
					blockHoursTwo += '<li class="p-line"><span class="p-day">Неділя</span>  <span class="close-color">Зачинено</span></li>';
				}				
								
				if( blockHoursTwo != '' ) {
					blockHours = '<div class="p-hours fs-12 pl-5">'+
						'<p><i class="icon-clock-o fs-14 pr-5 p-icon2"></i>'+ res.hours.message +'</p><ul>';
					blockHours += blockHoursTwo;	
					blockHours += '</ul></div>';	
				}
																													
			}
			blockHours += '<hr class="p-hr">';
			
			var pComment = res.rate_average;
				pComment += res.block_comments;

				
			var pReview = res.block_review;
				
			// get place-content			
			var placeContent = ''+
				blockSlides +
				blockHead +
				pAddress +
				blockHours +
				res.inform +
				pComment +
				pReview;
						
			$('#place-content').append(placeContent);                   
			$('#place-content').show();
			
			$('.carousel').carousel({
			  interval: false
			});
			var heightPlaceContent = (ivh > 500) ? 480 : ivh - 20 ;
			//heightPlaceContent = (ivh < 500) ? ivh - 20 :  ;
			$("#place-content").mCustomScrollbar({
				setHeight: heightPlaceContent,//500,
				theme:"minimal-dark",
				advanced:{
					updateOnContentResize: false,
					updateOnImageLoad: false
				}
			});
			$('#place-content').append('<i id="close-place-content" class="icon-close" onclick="placeContentHide()"></i>');
			return false;
		});			
}
// get and show comments
function ivShowComments(the, postId) {
	var ajaxdata = {
		action : 'iv_ajax_get_comments',
		nonce : ivPlacesAjax.nonce,
		post_id: postId
	};
	$.post( ivPlacesAjax.url, ajaxdata, function( response ) {			
		$('#iv-all-comments').html(response);		
		$('#place-content').mCustomScrollbar("update");			
	});
	return false;
}
// show comment form
function ivShowCommentForm(the, postId) {
	var ajaxdata = {
		action : 'iv_ajax_get_form',
		nonce : ivPlacesAjax.nonce,
		post_id: postId
	};
	$.post( ivPlacesAjax.url, ajaxdata, function( response ) {	
		$('#comment-result').html(response);		
		$('#place-content').mCustomScrollbar("update");					
	});
	return false;
}

// Hide place-content
function placeContentHide() {
	$("#place-content").mCustomScrollbar("destroy");
	$('#place-content').html("");                   
	$('#place-content').hide();
}
// Show full contact information
function ivShowContactInfo() {
	$('.hide-ci').hide();
	$('.show-ci').show();
	$('#place-content').mCustomScrollbar("update");
}

function ivShowWeatherWidget() {
	$('#plw-mini').hide();
	$('#plw-widget').show();
}

function ivShowWeatherMini() {
	$('#plw-widget').hide();
	$('#plw-mini').show();
}

$('.card_toggle').click(function() {  
	$(this).css('display', 'none');
	$(this).siblings('.card_toggle-2').css('display', 'block');
	$(this).parent().parent().siblings('.w-day-details').css('display', 'block');
	$("#plw-widget").mCustomScrollbar("update");
	return false;
});

$('.card_toggle-2').click(function() {  
	$(this).css('display', 'none');
	$(this).siblings('.card_toggle').css('display', 'block');
	$(this).parent().parent().siblings('.w-day-details').css('display', 'none');
	$("#plw-widget").mCustomScrollbar("update");
	return false;
});
// window onload
$(window).load(function() {
	var d = new Date();
	var dm = d.getMonth();
	for(var i = 0; i <= 9; i++) {
		if(dm == i) { return; }
	}
	ivw = $(window).width();
	let window_height = $(window).height();
	let header_height = $('#header').height();
	
	ivh = (window_height - header_height);
	
	$('#iv-window').css({'width': ivw, 'height': ivh});
	$('.cat-menu').css({'height': ivh});
	// for theme directorybox
	$('.main-section').css({minHeight: ivh});
	// for media (max-width: 768px) {
	if(ivw < 769) {
		$('.cat-icon').mouseover(function(){
			$('.cat-menu').css({'width': '300px'});
			$('.cat-name').css({'width': '220px','height': '40px','line-height': '40px','display': 'inline-block'});
		});
		$('.cat-menu').mouseleave(function(){
			$('.cat-name').css('display', 'none');
			$(this).css('width', '50px');	
		});
		
		setSwipeForCatIcon();
	}
	// begin
	$('.link-category:first').click();	
		
	// Scroll of menu	
	$(".cat-menu").mCustomScrollbar({
		setHeight: ivh,
		theme: "minimal-dark"
	});
	
	// Scroll of weather widget	
	if(ivw < 577) {
		var plwWidgetHeight = (ivh - 50);
	}
	$("#plw-widget").mCustomScrollbar({
		setHeight: plwWidgetHeight,
		theme: "minimal-dark"
	});	
});

function setSwipeForCatIcon() {
	var el_test = document.getElementsByClassName('cat-icon');
	
	for (var i = 0; i < el_test.length; i++) {
		var initialPoint;
		var finalPoint;
		el_test[i].addEventListener('touchstart', function(event) {
			initialPoint=event.changedTouches[0];
		}, false);
		
		el_test[i].addEventListener('touchend', function(event) {
			finalPoint=event.changedTouches[0];
			var xAbs = Math.abs(initialPoint.pageX - finalPoint.pageX);
			var yAbs = Math.abs(initialPoint.pageY - finalPoint.pageY);
			if (xAbs > 20 || yAbs > 20) {
				if (xAbs > yAbs) {
					if (finalPoint.pageX < initialPoint.pageX){
						/*swipe left*/
						catMenuOpen = false;
						$('.cat-name').css('display', 'none');
						$('.cat-menu').css('width', '50px');
						return false;
					} else {
							/*swipe right*/
							catMenuOpen = true;																																																
							$('.cat-menu').css({'width': '300px'});
							$('.cat-name').css({'width': '220px','height': '40px','line-height': '40px','display': 'inline-block'});																														
							return false;
						}
				} else {
					if (finalPoint.pageY < initialPoint.pageY){
						/*swipe up*/
						return false;
					} else{
						/*swipe down*/
						return false;
					}
				}
			}
		}, false);
	}
}
</script>

