��    V      �     |      x     y     �     �     �     �     �     �  
   �  	     
                  6     =  #   L  
   p     {     �     �     �     �  "   �     �     	     &	     2	     7	     >	     M	     R	     ^	  	   c	     m	     �	     �	     �	     �	     �	     �	     �	  
   �	     �	     �	     
     *
     9
     F
     \
     t
     {
     �
     �
     �
     �
     �
     �
  )   �
               4  
   A     L     ]  	   k     u  0   �     �     �     �     �  $     
   6     A     S     e          �     �     �     �     �     �     �     �  	   �  u  �  B   <  3     (   �      �     �     
  $   %     J     \     j     {  ,   �     �     �  -   �  
     '   !  .   I     x  2   �  <   �  H   �  9   B  /   |     �     �     �     �     �                  (   4     ]     }  (   �     �  
   �     �          %  
   E      P  2   q     �     �  #   �  '   �          0  	   E     O  *   g  D   �  .   �  !     W   (     �  .   �  +   �     �  "        %     B  C   R  v   �               /  +   >  =   j     �  "   �     �  $   �     #     3     F     S     `     m     r  
   �     �     �            7   5   B   )      #   4   Q          P      =   :      U       2   /   V       ?      @   9           1       C       *   
                                   +      -                    <   .      8   $             J       R      ,       0   '   S   >       6   (   D                       K          "          L               O   T   %   &   H                           	            !          F   A   M   3       N                        ;       I   E          G    (Click to add opening hours) Add New place category Add new number Add new place Address All comments:  All place categories All places Average:  Been here? COMMENT Click here for removing Closed Color category Color category and icon on frontend Color name Contact information Disable working hours EMAIL Edit place category Error adding a comment. Error: Check fields (Name, Email). Error: email is not correct. Featured Image Find on map Icon Images Link Your site Logo Menu-Places NAME New place New place category name No comments No contact information. No phone number Opening hours Place Place categories Place category Place edit Places Places not found Places not found in Trash Places options Post Comment Post does not define! Rating does not define! Remove Round the clock STOP! Search place Search place category Set featured image Set working hours Show contact info. Sorry, comments are closed for this item. Total:  Update place category Upload image View place Write a comment. Write review. YOUR SITE You must sign in to vote. You need to register or log in to post comments. Your Facebook page Your Instagram page Your Twitter page Your comment is adding. Your comment is awaiting moderation. Your email Your phone number Your vote adding. Your vote already exists. friday monday saturday sunday thursday to tuesday vote votes wednesday Project-Id-Version: my-plugin-name
POT-Creation-Date: 2018-12-03 17:22+0200
PO-Revision-Date: 
Last-Translator: Myname <myemail@gmail.com>
Language-Team: My Super Team
Language: ru
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=3; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
X-Poedit-SourceCharset: UTF-8
X-Poedit-KeywordsList: __;_e;_x:1,2c;_ex:1,2c;_n:1,2;_nx:1,2,4c;_n_noop:1,2;_nx_noop:1,2,3c;esc_attr__;esc_attr_e;esc_html__;esc_html_e
X-Poedit-Basepath: ..
X-Generator: Poedit 1.8.7.1
X-Poedit-SearchPath-0: .
 (Натисніть щоб додати робочі години) Додати нову категорію місць Додати номер телефону Додати нове місце Адреса Всі коментарі: Всі категорії місць Всі місця Серенє: Були тут? Коментар Натисніть для видалення Зачинено Колір категорії Колір категорії і іконки Колір Контактна інформація Відключити робочі години Ел. адрес Редагувати категорію місць Помилка при додаванні коментаря. Помилка: перевірте поля (Ім'я, Ел. адрес). Помилка: ел. адрес не коректний. Рекомендоване зображення Знайти на карті Іконка Зображення Ваш сайт Лого Меню Місця Ім'я Нове місце Назва нової категорії Немає коментарів Немає контактів Немає номера телефону Робочі години Місце Категорії місць Категорія місць Редагувати місце Місця Місць не знайдено В корзині місць не знайдено Опції місць Опублікувати Запис не визначено! Рейтинг не визначено! Видалити Цілодобово Стоп! Знайти місце Знайти категорію місць Встановити рекомендоване зображення Встановити робочі години Показати контакти Вибачте, коментарі для цього запису відключені. Загалом: Обновити категорію місць Завантажити зображення Переглянути Написати коментар. Напишіть огляд. Ваш сайт Ви повинні увійти щоб проголосувати. Вам потрібно зареєструватись або увійти щоб залишати коментарі. Фейсбук Інстаграм Твіттер Ваш коментар добавлено. Ваш коментар очікує на модерацію. Ел. адреса Ваш номер телефону Ваш голос додано. Ваш голос вже існує. п'ятниця понеділок субота неділя четвер до вівторок голос голоів середа 