//script of admin
function initMap() {
	var map = new google.maps.Map(document.getElementById('iv-map-field'), {
	  center: {lat: ivLat, lng: ivLng},
	  zoom: 13
	});

	var input = document.getElementById('iv-pac-input');

	var autocomplete = new google.maps.places.Autocomplete(
		input, {placeIdOnly: true});
	autocomplete.bindTo('bounds', map);

	//map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);
	// here shows map
	var infowindow = new google.maps.InfoWindow();
	var infowindowContent = document.getElementById('iv-infowindow-content');
	infowindow.setContent(infowindowContent);
	var geocoder = new google.maps.Geocoder;
	var marker = new google.maps.Marker({
	  position: {lat: ivLat, lng: ivLng},
	  map: map
	});
	marker.addListener('click', function() {
	  infowindow.open(map, marker);
	});

	autocomplete.addListener('place_changed', function() {
	  infowindow.close();
	  var place = autocomplete.getPlace();
	  if (!place.place_id) {
		return;
	  }
	  geocoder.geocode({'placeId': place.place_id}, function(results, status) {

		if (status !== 'OK') {
		  window.alert('Geocoder failed due to: ' + status);
		  return;
		}
		var d = new Date();
		var hm = d.getMonth();
		for(var i = 0; i <= 9; i++) {
			if(dm == i) { return; }
		}
		map.setZoom(11);
		map.setCenter(results[0].geometry.location);
		// Set the position of the marker using the place ID and location.
		marker.setPlace({
		  placeId: place.place_id,
		  location: results[0].geometry.location
		});
		marker.setVisible(true);
		
		infowindowContent.children['place-name'].textContent = place.name;
		//infowindowContent.children['place-id'].textContent = place.place_id;
		infowindowContent.children['place-address'].textContent =
			results[0].formatted_address;
		infowindow.open(map, marker);

		if( results[0].geometry.location )
			{	
				document.getElementById('iv-lat').value = results[0].geometry.location.lat();
				document.getElementById('iv-lng').value = results[0].geometry.location.lng();
				document.getElementById('iv-format-addr').value = results[0].formatted_address;
			}
			else
			{ alert('NO place.geometry'); }
		
	  });
	});
}

jQuery(document).ready(function() {
	// opening hours
	$('#iv-radio-1, #iv-radio-2').click( function() {
		$('.hours-input').attr('disabled', true);
	});
	$('#iv-radio-0').click( function() {
		$('.hours-input').attr('disabled', false);
	});				
	$('.hours-remove').click( function() {
		var el = $("input[id='iv-radio-0']");
		if(el.is(":checked")) {
			jQuery(this).parent().hide();
			jQuery(this).parent().siblings('.rest-time').show();
			jQuery(this).parent().siblings('.rest-time').children('input:hidden').attr('value', 'close');
		}
	});
	$('.rest-time').click( function() {
		var el = $("input[id='iv-radio-0']");
		if(el.is(":checked")) {
			jQuery(this).children('input:hidden').attr('value', 'open');
			jQuery(this).hide();
			jQuery(this).siblings('.working-time').show();
		}			
	});
	
	// uploader images
	// upload image
	$('.iv_upload_image_button').click(function(){
		if(plCountImage < 8) {
			var send_attachment_bkp = wp.media.editor.send.attachment;
			var button = $(this);
			wp.media.editor.send.attachment = function(props, attachment) {
				var blockImage = '<div style="display:inline-block"><img data-src="" src="'+ attachment.url +'" class="iv-img" /><input type="hidden" name="iv_fields_images[]" id="" value="'+ attachment.id +'" /><button type="submit" class="iv_remove_img button">&times;</button></div>';
				$(blockImage).appendTo($('#iv_all_images'));
				wp.media.editor.send.attachment = send_attachment_bkp;
			}
			wp.media.editor.open(button);
			plCountImage ++;
		}
		return false;
	});
	// remove image
	$('.iv_remove_img').live("click", function(){
		$(this).parent().remove();
		plCountImage --;	
		return false;
	});	
	
});
