(function($) {

    $(document).on('mouseover','.vote-block-2 li',function() {
        var $el = $(this);
        var star = parseInt($el.text(),10);

        if($el.parent().parent().hasClass('disabled')) {
            return false;
        }
        
        $('#rating-info').show().html(star +' ' + decOfNum(star, [tVote, tVotes, tVotes]));
    }).on('mouseleave','.vote-block-2 li',function() {
        $('#rating-info').hide();
    });
    $(document).on('click','.vote-block-2 li',function() {
		$('#res-info').hide();
        var $el = $(this);
        var id = $el.parent().parent().data('id');
        var total = $el.parent().parent().data('total');
        var rating = $el.parent().parent().data('rating');
        var num = parseInt($el.text(),10);
        var nonce = $el.parent().parent().data('rate-nonce');

        if($el.parent().parent().hasClass('disabled')) {
            return false;
        }

        $.ajax({
            type: 'POST',
            url: ivPlacesAjax.url,          
            data: {id:id, num:num, action:'iv_ajax_send_rating', nonce_code:nonce},
            success: function(pr) {
				var jsonPR = JSON.parse(pr);			
                if( typeof jsonPR.error != 'undefined' && jsonPR.error.length > 0) {
					//alert(jsonPR.error);
					$('#rate-info').show().html(jsonPR.error);
					$el.parent().parent().addClass('disabled');
                    return false;
                } else {
					//alert(jsonPR.success);
                    $el.parent().parent().addClass('disabled');
                    $('.vote-block').find('.current span').css('width',jsonPR.interest +'%');
                    total++;
                    var abs = ((rating+num)/total);
                    abs = (abs^0)===abs?abs:abs.toFixed(1);
                    $('#rate_abs').text(abs);
                    $('#rate-info').show().html(jsonPR.success);
                }
            }
        });

        return false;
    });

})(jQuery);

function decOfNum(number, titles) {  
    cases = [2, 0, 1, 1, 1, 2];  
    return titles[ (number%100>4 && number%100<20)? 2 : cases[(number%10<5)?number%10:5] ];  
}  

