jQuery.extend(jQuery.fn, {
	// check length of field
	checka: function () {
		if (jQuery(this).val().length < 3) {jQuery(this).addClass('error');return false} else {jQuery(this).removeClass('error');return true}
	},
	// check email
	checke: function () {
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		var emailaddressVal = jQuery(this).val();
		if (!emailReg.test(emailaddressVal) || emailaddressVal == "") {
			jQuery(this).addClass('error');return false
		} else {
			jQuery(this).removeClass('error');return true
		}
	},
});
// add comment
jQuery(function($){
	$(".nameinput").live('focus', function() { $(this).removeClass('error'); });
	$(".emailinput").live('focus', function() { $(this).removeClass('error'); });
	$("textarea").live('focus', function() { $(this).removeClass('error'); });
	
	$('#ivsubmit').live('click', function(e) {
		//alert('YES');
		e.preventDefault();
		// check logged
		if($("#author").length) var author = $("#author").checka();
		if($("#email").length) var email = $("#email").checke();
		var comment = $("#comment").checka();
		// check fields and ajax request
		if (!$('#submit').hasClass('loadingform') && !$("#author").hasClass('error') && !$("#email").hasClass('error') && !$("#comment").hasClass('error')){
			var ajaxdata = {
				action : 'iv_ajax_add_comment',
				author: $("#author").attr('value'),
				email: $("#email").attr('value'),
				url: $("#url").attr('value'),
				comment: $("#comment").attr('value'),
				comment_post_ID: $("#comment_post_ID").attr('value'),
				comment_parent: $("#comment_parent").attr('value')
			};
			$('#ivsubmit').addClass('loadingform').val('Loading');
			jQuery.post( ivPlacesAjax.url, ajaxdata, function( response ) {	
				$('#ivsubmit').removeClass('loadingform').val('Коментувати');
				$('#comment-result').html("");
				$('#comment-result').html(response);
				
				$('#place-content').mCustomScrollbar("update");	
				return false;
			});
				
		}
		
	});
	

});

