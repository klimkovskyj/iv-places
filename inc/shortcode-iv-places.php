<?php if( !defined('WPINC') ) die;

/** Core class. */
if(! class_exists('Shortcode_Iv_Places')){

	class Shortcode_Iv_Places {
		
		protected $_mm = '';
		protected $_mс = '';
			
		/** Initialize the plugin. */
		public function __construct() {	
			$this->_mm = __( 'мм', 'iv-places' );
			$this->_mc = __( 'м/с', 'iv-places' );
			
			add_shortcode('iv_places', array($this, 'iv_shortcode_callback'));
			//add_action('init', array($this, 'iv_register_taxonomy'));
			add_action('wp_enqueue_scripts', array( $this, 'iv_frontend_scripts_method'), 99);			
			add_action('wp_enqueue_scripts', array($this, 'iv_ajax_data'), 99 );
		}

		/** Shortcode view on frontend */
		public function iv_shortcode_callback() {
			wp_enqueue_script('places-mCustomScrollbar');
			wp_enqueue_script('places-carousel');
			wp_enqueue_script('places-util');
			wp_enqueue_script('places-comments');
			wp_enqueue_script('places-rating');
			wp_enqueue_script('places-google-map');		
			wp_enqueue_script('places-richmarker');
			wp_enqueue_script('places-markerclusterer');
			
			ob_start();
			date_default_timezone_set('Europe/Kiev');
			include( IVPL_DIR_PATH . 'templates/template.php' );
			$output = ob_get_contents();
			ob_end_clean();
 			return $output; // never echo or print in a shortcode!
		}
		
		/** Connect script and style on frontend */
		public function iv_frontend_scripts_method() {
			wp_deregister_script( 'wp-dp-google-map-api');
			
			wp_register_script('places-mCustomScrollbar', IVPL_DIR_URL . 'js/jquery.mCustomScrollbar.concat.min.js', array('jquery'), '', 'in_footer');
			wp_register_script('places-carousel', IVPL_DIR_URL . 'js/carousel.js', array('jquery'), '', 'in_footer');
			wp_register_script('places-util', IVPL_DIR_URL . 'js/util.js', array('jquery'), '', 'in_footer');
			wp_register_script('places-comments', IVPL_DIR_URL . 'js/places-comments.js', array('jquery'), '', 'in_footer');
			wp_register_script('places-rating', IVPL_DIR_URL . 'js/rating.js', array('jquery'), '', 'in_footer');
			wp_register_script('places-google-map', 'https://maps.googleapis.com/maps/api/js?key='. IVPL_GMAPS_API_KEY .'&libraries=places', '', '', 'in_footer');
			wp_register_script('places-richmarker', IVPL_DIR_URL . 'js/richmarker.js', array('places-google-map'), '', 'in_footer');
			wp_register_script('places-markerclusterer', IVPL_DIR_URL . 'js/markerclusterer.js', array('places-google-map'), '', 'in_footer');
						
			wp_enqueue_style('places-frontend', IVPL_DIR_URL . 'css/places-frontend.css');
			wp_enqueue_style('places-mCustomScrollbar', IVPL_DIR_URL . 'css/jquery.mCustomScrollbar.css');	
			wp_enqueue_style('places-carousel', IVPL_DIR_URL . 'css/carousel.css');
			wp_enqueue_style('places-rating', IVPL_DIR_URL . 'css/rating.css');
			wp_enqueue_style('places-weather', IVPL_DIR_URL . 'css/weather.css');
			//icons
			wp_enqueue_style('places-mapnearby', IVPL_DIR_URL . 'icons/mapnearby/style.css');
			wp_enqueue_style('places-custom-fileds', IVPL_DIR_URL . 'icons/custom-fileds/style.css');
			wp_enqueue_style('places-listingtype', IVPL_DIR_URL . 'icons/listingtype/style.css');
			wp_enqueue_style('places-icon_category', IVPL_DIR_URL . 'icons/icon_category/style.css');
			wp_enqueue_style('places-default', IVPL_DIR_URL . 'icons/default/style.css');
		}
		
		function iv_ajax_data() {
			wp_localize_script('places-mCustomScrollbar', 'ivPlacesAjax', array('url' => admin_url('admin-ajax.php'), 'nonce' => wp_create_nonce('ivplaces-ajax-nonce')));  
		}
	}
}

new Shortcode_Iv_Places();
