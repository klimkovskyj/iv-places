<?php
if( !function_exists('get_temperature_plus') ) {
	/* @return string $temperature */
	function get_temperature_plus($temperature) {
		$res_temperature = round($temperature);
		if ($res_temperature == 0) { return abs($res_temperature); } elseif ($res_temperature > 0) { return '+'.$res_temperature; } else { return $res_temperature; }
	}
}
if( !function_exists('get_day') ) {
	/* @return string $day_name */
	function get_day($number_day) {
		$days_of_week = array( __( 'неділя', 'iv-places' ), __( 'понеділок', 'iv-places' ), 
								__( 'вівторок', 'iv-places' ), __( 'середа', 'iv-places' ), 
								__( 'четвер', 'iv-places' ), __( 'п\'ятниця', 'iv-places' ), 
								__( 'субота', 'iv-places' ) );
		return $days_of_week[ esc_attr( $number_day ) ];
	}
}
if( !function_exists('get_short_day') ) {
/* @return string $short_day_name */
	function get_short_day($number_day) {
		$short_days = array( __( 'нд', 'w-f-d' ), __( 'пн', 'w-f-d' ), 
								__( 'вт', 'w-f-d' ), __( 'ср', 'w-f-d' ), 
								__( 'чт', 'w-f-d' ), __( 'пт', 'w-f-d' ), 
								__( 'сб', 'w-f-d' ) );
		return $short_days[ esc_attr( $number_day ) ];
	}
}
if( !function_exists('get_month') ) {
	/* @return string $month_name */
	function get_month($number_month) {
		$month = array('',  __( 'січня', 'iv-places' ), __( 'лютого', 'iv-places' ), 
								__( 'березня', 'iv-places' ), __( 'квітня', 'iv-places' ), 
								__( 'травня', 'iv-places' ), __( 'червня', 'iv-places' ), 
								__( 'липня', 'iv-places' ), __( 'серпня', 'iv-places' ),
								__( 'вересня', 'iv-places' ), __( 'жовтня', 'iv-places' ), 
								__( 'листопада', 'iv-places' ), __( 'грудня', 'iv-places' ) );
		return $month[ esc_attr( $number_month ) ];
	}
}

/* @return string $part_day */
if( !function_exists('get_part_day') ) {
	function get_part_day($hour) {
		$hour = abs($hour);
		switch($hour) {
			case ($hour >= 23): return __( 'Вночі', 'iv-places' ); break;
			case ($hour < 5): return __( 'Вночі', 'iv-places' ); break;
			case ($hour < 11 && $hour >= 5): return __( 'Вранці', 'iv-places' ); break;
			case ($hour < 17 && $hour >= 11): return __( 'Вдень', 'iv-places' ); break;
			case ($hour < 23 && $hour >= 17): return __( 'Ввечері', 'iv-places' ); break;
			
			default: return __( 'Сьогодні', 'iv-places' ); break;
		}
	}
}
if( !function_exists('get_wind_class') ) {
	/* @return string $wind_direction icon */
	function get_wind_class($direction) {	
		switch(true) {
			case ($direction < 22 && $direction >= 0): return '_S'; break;
			case ($direction <= 360 && $direction > 338): return '_S'; break;
			
			case ($direction < 68 && $direction >= 22): return '_SW'; break;
			case ($direction < 112 && $direction >= 68): return '_W'; break;
			case ($direction < 158 && $direction >= 112): return '_NW'; break;
			case ($direction < 202 && $direction >= 158): return '_N'; break;
			case ($direction < 248 && $direction >= 202): return '_NE'; break;
			case ($direction < 292 && $direction >= 248): return '_E'; break;
			case ($direction < 338 && $direction >= 292): return '_SE'; break;
			
			default: return 'm-box__icon-wind'; break;
		}
	}
}
