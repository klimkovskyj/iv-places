<?php if( !defined('WPINC') ) die;

/** Submenu Setting. */
if(! class_exists('Iv_Submenu_Setting')){
	
	class Iv_Submenu_Setting {
			
		/** Initialize the plugin. */
		public function __construct() {
			// add submenu Setting to menu iv_places
			add_action('admin_menu', array($this, 'iv_submenu_page'));
		}
		
		public function iv_submenu_page() {
			add_submenu_page('edit.php?post_type=iv-places', __( 'Налаштування', 'iv-places' ), __( 'Налаштування', 'iv-places' ), 'manage_options', 'iv-places-submenu-page', array($this, 'iv_submenu_page_callback')); 
			
			add_action( 'admin_init', array( $this, 'iv_register_settings' ) );
		}
		
		public function iv_submenu_page_callback() {
			// content page setting
			?>
			<div class="wrap">
				<h2><?php _e( 'Налаштування', 'iv-places' ); ?></h2>
				
				<form method="post" action="options.php">
					<?php settings_fields( 'iv-settings-group' ); ?>
					<?php $iv_options = get_option( 'iv_places_options' ); ?>
					
					<table class="form-table">
						
						<tr valign="top">
							<th scope="row">Darksky Api Key</th>
							<td><input type="text" name="iv_places_options[darksky_key]" id="darksky_key" placeholder="9a04d5703780332dadaa95ec276334a6" 
								value="<?php echo esc_attr( !empty($iv_options['darksky_key'])?$iv_options['darksky_key']:'' ); ?>" size="35" />					
							</td>
						</tr>
						
						<tr valign="top">
							<th scope="row">Google Maps Api Key</th>
							<td><input type="text" name="iv_places_options[googl_maps_key]" id="googl_maps_key" placeholder="AIzaSyA9Pe8AdlnGTIdRysmdWy3HzHNEFk5xuOc" 
								value="<?php echo esc_attr( !empty($iv_options['googl_maps_key'])?$iv_options['googl_maps_key']:'' ); ?>" size="35" />					
							</td>
						</tr>
						
						<tr valign="top">
						<th scope="row"><?php _e( 'Назва місця', 'iv-places' ); ?></th>
						<td><input type="text" name="iv_places_options[city_name]" id="city-search" placeholder="e.g. <?php _e( 'Новий Розділ', 'iv-places' ); ?>" 
							value="<?php echo esc_attr( !empty($iv_options['city_name'])?$iv_options['city_name']:'' ); ?>" />					
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row">Latitude</th>
						<td><input type="text" name="iv_places_options[latitude]" id="latitude" placeholder="e.g. 49.4696"
							value="<?php echo esc_attr( !empty($iv_options['latitude'])?$iv_options['latitude']:'' ); ?>" />					
						</td>
					</tr>
					
					<tr valign="top">
						<th scope="row">Longitude</th>
						<td><input type="text" name="iv_places_options[longitude]" id="longitude" placeholder="e.g. 24.1386"
							value="<?php echo esc_attr( !empty($iv_options['longitude'])?$iv_options['longitude']:'' ); ?>" />					
						</td>
					</tr>
					</table>
					
					<p class="submit">
						<input type="submit" class="button-primary" value="<?php _e( 'Зберегти', 'iv-places' ); ?>" />
					</p>
				</form>
			</div>
			<?php
		}
		
		/* Register settings of submenu Setting */
		public function iv_register_settings() {
			register_setting( 'iv-settings-group', 'iv_places_options', array( $this, 'iv_places_sanitize_options') );
		}
		
		/* Sanitize options of submenu Setting */
		public function iv_places_sanitize_options( $input ) {
			$input['darksky_key'] = sanitize_text_field( $input['darksky_key'] );
			$input['googl_maps_key'] = sanitize_text_field( $input['googl_maps_key'] );
			$input['city_name'] = sanitize_text_field( $input['city_name'] );
			$input['latitude'] = sanitize_text_field( $input['latitude'] );
			$input['longitude'] = sanitize_text_field( $input['longitude'] );
			return $input;
		}
		
	}
	
}

new Iv_Submenu_Setting();
