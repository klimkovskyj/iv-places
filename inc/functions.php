<?php if( !defined('WPINC') ) die;
/**
 * 	MENU
 */
 
// register menu for frontend
function iv_setup() {
	register_nav_menus(array(
		'iv-places-menu' => __('Menu-Places', 'iv-places'),
	));
}
add_action('after_setup_theme', 'iv_setup');

/**
 * POSTS 
 */
 
// ajax get posts from taxonomy 
add_action( 'wp_ajax_nopriv_iv_ajax_get_posts', 'iv_ajax_get_posts' );
add_action( 'wp_ajax_iv_ajax_get_posts', 'iv_ajax_get_posts' );
function iv_ajax_get_posts() {
	$nonce = $_POST['nonce'];
	$cat_id = intval($_POST['cat_id']);

	// check ajax
	if (!wp_verify_nonce( $nonce, 'ivplaces-ajax-nonce' ))
		die ( 'Stop!');
	
	$response = array();	
	$args = array( 'tax_query' => array( array('taxonomy' => 'iv-cat-places',
												'field'    => 'id',
												'terms'    => array( $cat_id )
											)
					),
					'post_type' => 'iv-places',
					'posts_per_page' => -1
		);
	$posts = get_posts( $args );

	foreach( $posts as $post ){
		$map_data = get_post_meta($post->ID, 'iv_fields_map', true);
		if (!empty($map_data['address'])) {
			if (!empty($map_data['latitude']) && !empty($map_data['longitude'])) {
				$response[] = array('post_id' => $post->ID, 'map_data' => $map_data, 'post_title' => $post->post_title);
			}
		}
	}

	echo json_encode($response);
	exit;
}

// ajax get chosen post
add_action( 'wp_ajax_nopriv_iv_ajax_get_post', 'iv_ajax_get_post' );
add_action( 'wp_ajax_iv_ajax_get_post', 'iv_ajax_get_post' );
function iv_ajax_get_post() {
	$nonce = $_POST['nonce'];
	$post_id = intval($_POST['post_id']);

	// check ajax
	if (!wp_verify_nonce( $nonce, 'ivplaces-ajax-nonce' ))
		die ( 'Stop!');
	$response = array();
	$_post = get_post($post_id);
	if (!empty($_post))	{
		$response['title'] = $_post->post_title;
		$url_logo = get_post_meta($post_id, 'iv_fields_logo', true);
		if (!empty($url_logo)) {
			$response['logo'] = wp_get_attachment_url($url_logo);
		}
		$images_data = get_post_meta($post_id, 'iv_fields_images', true);
		if(!empty($images_data)) {
			foreach($images_data as $image) {
				if(!empty($image)) {
					$response['images'][] = wp_get_attachment_url($image);					
				}		
			}
		}		
		$map_data = get_post_meta($post_id, 'iv_fields_map', true);
		if (!empty($map_data['address'])) {
			$response['address'] = $map_data['address'];
		}
		$radio_data = get_post_meta($post_id, 'iv-radio', true);
		if(!empty($radio_data) && $radio_data == 'round') {
			$response['hours']['radio'] = 'round';
			$response['hours']['message'] = __('Round the clock', 'iv-places');
		}
		if(!empty($radio_data) && $radio_data == 'set') {
			$hour_data = get_post_meta($post_id, 'iv_fields_hours', true);
			$response['hours']['radio'] = 'set';
			$response['hours']['message'] = __('Opening hours', 'iv-places');
			$response['hours']['data'] = $hour_data;
		}
		if(!empty($radio_data) && $radio_data == 'disable') {
			$response['hours']['radio'] = 'dis1';
		}
		// contact inform short
		$inform_data = get_post_meta($post_id, 'iv_fields_inform', true);
		$temp_phone = $inform_data['phone'][0];
		if( $temp_phone ) {
			$temp_phone = substr($temp_phone, 0, 15);			
		} else {
			$temp_phone = __('No phone number', 'iv-places');
		}
		$phone_1 = '<div class="ml-5 hide-ci"><i class="icon-phone4 fs-12 pr-5 p-icon2"></i> '. $temp_phone .'</div>';
		$phone_1 .= '<p class="ml-5 hide-ci ta-c"><span class="link-comment" onclick="ivShowContactInfo()">'. __('Show contact info.', 'iv-places') .'</span></p>';		
		
		// contact inform full
		$full_info = '';
		foreach( $inform_data['phone'] as $number ) {
			if( !empty($number) ) {
				$full_info .= '<div class="ml-5 show-ci"><i class="icon-phone4 fs-12 pr-5 p-icon2"></i> '. $number .'</div>';
			}
		}
		if( !empty($inform_data['link_site']) ) {
			$full_info .= '<div class="ml-5 show-ci"><i class="icon-invoice-user-account fs-12 pr-5 p-icon2"></i> '. $inform_data['link_site'] .'</div>'; 
		}
		if( !empty($inform_data['link_email']) ) {
			$full_info .= '<div class="show-ci"><a class="ml-5" href="mailto:'. $inform_data['link_email'] .'" target="_blank"><i class="icon-email fs-12 pr-5 p-icon2"></i> '. $inform_data['link_email'] .'</a></div>';
		}
		if( !empty($inform_data['link_facebook']) ) {
			$full_info .= '<div class="show-ci"><a href="'. $inform_data['link_facebook'] .'" target="_blank"><i class="icon-facebook-stroke fs-12 pr-5 pl-5 p-icon2"></i> '. $inform_data['link_facebook'] .'</a></div>';
		}
		if( !empty($inform_data['link_twitter']) ) {
			$full_info .= '<div class="show-ci"><a href="'. $inform_data['link_twitter'] .'" target="_blank"><i class="icon-twitter-stroke fs-12 pr-5 pl-5 p-icon2"></i> '. $inform_data['link_twitter'] .'</a></div>'; 
		}
		if( !empty($inform_data['link_instagram']) ) {
			$full_info .= '<div class="show-ci"><a href="'. $inform_data['link_instagram'] .'" target="_blank"><i class="icon-instagram-photo-camera-stroke fs-12 pr-5 pl-5 p-icon2"></i> '. $inform_data['link_instagram'] .'</a></div>';
		}
		if( empty($full_info) ) {
			$full_info = '<div class="ml-5 show-ci">'. __('No contact information.', 'iv-places') .'</div>';
		}
		$response['inform'] = '<div class=" fs-14 pl-5">'. $phone_1 . $full_info .'</div><hr class="p-hr">';
		
		
		// first comment
		$args_first = array(
			'number' => 1,
			'orderby' => 'comment_date',
			'order' => 'DESC',
			'post_id' => $post_id,
			'type' => '', // only comment, not ping
		);

		if( $first_comment = get_comments( $args_first ) ){
			$comment_content = $first_comment[0]->comment_content;
		} else {
			$comment_content = __('No comments', 'iv-places');
		}
		$response['block_comments'] = '<div class="fs-12 ml-10 cl-b">'. $comment_content .'</div>';
		// count comment
		$comments_count = wp_count_comments($post_id);
		if( $comments_count->approved > 0 ) {
			$response['block_comments'] .= '<div class="fs-12 ml-10 link-comment" onclick="ivShowComments(this, '. $post_id .');">'. __('All comments: ', 'iv-places') . $comments_count->approved .'</div>';
		}
		$response['block_comments'] .= '</div><hr class="p-hr"><div id="iv-all-comments"></div>';
		// rating
		$rate = iv_rating($post_id);
		$response['rate_abs'] = $rate['abs'];
		$response['rate_average'] = $rate['average'];
		$response['block_review'] = '<div class="rate-bottom">'. $rate['bottom'] .'</div><div id="rate-info" class="close-color ta-c cl-b"></div>';
		$response['block_review'] .= '<div class="p-review cl-b">'. __('Been here?', 'iv-places') .' <span class="link-comment" onclick="ivShowCommentForm(this, '. $post_id .')">'. __('Write review.', 'iv-places') .'</span></div>';
		$response['block_review'] .= '<div id="comment-result"></div><hr class="p-hr">';
	} else {
		$response['error'] = "Error";
	}
	echo json_encode($response);
	exit;
}

/**
 * COMMENTS
 */

// return comments of post
add_action('wp_ajax_iv_ajax_get_comments', 'iv_ajax_get_comments'); 
add_action('wp_ajax_nopriv_iv_ajax_get_comments', 'iv_ajax_get_comments');

function iv_ajax_get_comments() {
	$nonce = $_POST['nonce'];
	$post_id = intval($_POST['post_id']);

	// check ajax
	if (!wp_verify_nonce( $nonce, 'ivplaces-ajax-nonce' ))
		die ( 'Stop!');
	$output = '<ol class="commentlist" style="width:350px;">';

		// get comments
		$comments = get_comments(array(
			'post_id' => $post_id,
			'status' => 'approve'
		));

		// output comments
		$output .= wp_list_comments(array(
			'per_page' => 25, // pagination
			'callback' => 'iv_comment_callback', 
			'echo' => false
		), $comments);

	$output .= '</ol>';
	
	echo $output;
	exit;
}

// callback for every comment
function iv_comment_callback( $comment, $args, $depth ) {
	if ( 'div' === $args['style'] ) {
		$tag       = 'div';
		$add_below = 'comment';
	} else {
		$tag       = 'li';
		$add_below = 'div-comment';
	}

	$classes = ' ' . comment_class( empty( $args['has_children'] ) ? '' : 'parent', null, null, false );
	?>

	<<?php echo $tag, $classes; ?> id="comment-<?php comment_ID() ?>">
	<?php if ( 'div' != $args['style'] ) { ?>
		<div id="div-comment-<?php comment_ID() ?>" class="comment-body"><?php
	} ?>

	<div class="comment-author vcard">				
		<?php if ( $args['avatar_size'] != 0 ) {
			echo get_avatar( $comment, $args['avatar_size'] );
		}
		printf(
			__( '<cite class="fn">%s</cite> <span class="says">says:</span>' ),
			get_comment_author_link()
		);
		?>
	</div>

	<?php if ( $comment->comment_approved == '0' ) { ?>
		<em class="comment-awaiting-moderation">
			<?php _e( 'Your comment is awaiting moderation.', 'iv-places' ); ?>
		</em><br/>
	<?php } ?>

	<div class="comment-meta commentmetadata">
			<?php
			printf(
				__( '%1$s at %2$s' ),
				get_comment_date(),
				get_comment_time()
			); ?>
	</div>
	
	<?php comment_text(); ?> 

	<hr class="c-hr">
	<?php if ( 'div' != $args['style'] ) { ?>
		</div>
	<?php }

}

// get form for reply
add_action('wp_ajax_iv_ajax_get_form', 'iv_ajax_get_form'); 
add_action('wp_ajax_nopriv_iv_ajax_get_form', 'iv_ajax_get_form');
		
function iv_ajax_get_form(){
	$nonce = $_POST['nonce'];
	$post_id = intval($_POST['post_id']);

	// check ajax
	if (!wp_verify_nonce( $nonce, 'ivplaces-ajax-nonce' ))
		die ( 'Stop!');
	
	if ( comments_open($post_id) ) {	
		if (wp_get_current_user()->exists()) {
			$output = '<div id="respond" class="comment-respond">';
				//$output .= '<a id="cancel-comment-reply-link" style="display:none;" onclick="returnCommentForm()">Отменить ответ</a>';
				$output .= '<form id="commentform"  class="comment-form contact-form">';
					$output .= '<div class=""><strong>'. __('NAME', 'iv-places') .' *</strong><label><input class="nameinput" name="author" id="author" type="text" /></label></div>';
					$output .= '<div class=""><strong>'. __('EMAIL', 'iv-places') .' *</strong><label><input class="emailinput" name="email" id="email" type="text" /></label></div>';
					$output .= '<div class=""><strong>'. __('YOUR SITE', 'iv-places') .'</strong><label><input class="yoursiteinput" name="url" id="url" type="text" /></label></div>';
					$output .= '<div class=""><strong>'. __('COMMENT', 'iv-places') .'</strong><textarea name="comment" id="comment"></textarea></div>';
					$output .= '<input name="submit" type="submit" id="ivsubmit" value="'. __('Post Comment', 'iv-places') .'" />';
					$output .= '<input id="comment_post_ID" type="hidden" name="comment_post_ID" value="'. $post_id .'" />';
					$output .= '<input id="comment_parent" type="hidden" name="comment_parent" value="0" />';
				$output .= '</form>';
			$output .= '</div>';
			echo $output;
			exit;
		} else {
			wp_die( '<p class="com-error-res">'. __('You need to register or log in to post comments.', 'iv-places') .'</p>' );
		}
	} else {
		wp_die( '<p class="com-error-res">'. __('Sorry, comments are closed for this item.', 'iv-places') .'</p>' );
	}
}

// add comment by ajax
add_action('wp_ajax_iv_ajax_add_comment', 'iv_ajax_add_comment'); 
add_action('wp_ajax_nopriv_iv_ajax_add_comment', 'iv_ajax_add_comment');
		
function iv_ajax_add_comment() {
	global $wpdb;
	$comment_post_ID = isset($_POST['comment_post_ID']) ? (int) $_POST['comment_post_ID'] : 0;
 
	$post = get_post($comment_post_ID);
 
	if ( empty($post->comment_status) ) {
		do_action('comment_id_not_found', $comment_post_ID);
		exit;
	}
 
	$status = get_post_status($post);
 
	$status_obj = get_post_status_object($status);
 
	// check comment
	if ( !comments_open($comment_post_ID) ) {
		do_action('comment_closed', $comment_post_ID);
		wp_die( '<p class="com-error-res">'. __('Sorry, comments are closed for this item.', 'iv-places'). '</p>' );
	} elseif ( 'trash' == $status ) {
		do_action('comment_on_trash', $comment_post_ID);
		exit;
	} elseif ( !$status_obj->public && !$status_obj->private ) {
		do_action('comment_on_draft', $comment_post_ID);
		exit;
	} elseif ( post_password_required($comment_post_ID) ) {
		do_action('comment_on_password_protected', $comment_post_ID);
		exit;
	} else {
		do_action('pre_comment_on_post', $comment_post_ID);
	}
 
	$comment_author       = ( isset($_POST['author']) )  ? trim(strip_tags($_POST['author'])) : null;
	$comment_author_email = ( isset($_POST['email']) )   ? trim($_POST['email']) : null;
	$comment_author_url   = ( isset($_POST['url']) )     ? trim($_POST['url']) : null;
	$comment_content      = ( isset($_POST['comment']) ) ? trim($_POST['comment']) : null;
 
	// check logged
	$user = wp_get_current_user();
	if ( $user->exists() ) {
		if ( empty( $user->display_name ) )
			$user->display_name=$user->user_login;
		$comment_author       = $wpdb->escape($user->display_name);
		$comment_author_email = $wpdb->escape($user->user_email);
		$comment_author_url   = $wpdb->escape($user->user_url);
		$user_ID = get_current_user_id();
		if ( current_user_can('unfiltered_html') ) {
			if ( wp_create_nonce('unfiltered-html-comment_' . $comment_post_ID) != $_POST['_wp_unfiltered_html_comment'] ) {
				kses_remove_filters(); // start with a clean slate
				kses_init_filters(); // set up the filters
			}
		}
	} else {
		if ( get_option('comment_registration') || 'private' == $status )
			wp_die( '<p class="com-error-res">'. __('You need to register or log in to post comments.', 'iv-places') .'</p>' );
	}
 
	$comment_type = '';
 
	// check fields
	if ( get_option('require_name_email') && !$user->exists() ) {
		if ( 6 > strlen($comment_author_email) || '' == $comment_author )
			wp_die( '<p class="com-error-res">'. __('Error: Check fields (Name, Email).', 'iv-places') .'</p>' );
		elseif ( !is_email($comment_author_email))
			wp_die( '<p class="com-error-res">'. __('Error: email is not correct.', 'iv-places') .'</p>' );
	}
 
	if ( '' == trim($comment_content) ||  '<p><br></p>' == $comment_content )
		wp_die( '<p class="com-error-res">'. __('Write a comment.', 'iv-places') .'</p>' );
 
	// add new comment and get his
	$comment_parent = isset($_POST['comment_parent']) ? absint($_POST['comment_parent']) : 0;
	$commentdata = compact('comment_post_ID', 'comment_author', 'comment_author_email', 'comment_author_url', 'comment_content', 'comment_type', 'comment_parent', 'user_ID');
	$comment_id = wp_new_comment( $commentdata );
	// set coocie
	do_action('set_comment_cookies', $comment, $user);
 
	if( $comment_id ) {
		die( '<p class="com-success-res">'. __('Your comment is adding.', 'iv-places') .'</p>' );
	} else {
		die( '<p class="com-error-res">'. __('Error adding a comment.', 'iv-places') .'</p>' );
	}
}

/**
 * RATING 
 */
// add comment by ajax
if( wp_doing_ajax() ){
	add_action('wp_ajax_iv_ajax_send_rating', 'iv_ajax_send_rating'); 
	add_action('wp_ajax_nopriv_iv_ajax_send_rating', 'iv_ajax_send_rating');
}
function iv_ajax_send_rating() {
	$response = array();
	if( !isset($_POST['num']) ) {
		$response['error'] = __('Rating does not define!');	
		echo json_encode($response);
		exit;
	}
	if( !isset($_POST['id']) || !is_numeric($_POST['id']) ) {	
		$response['error'] = __('Post does not define!');
		echo json_encode($response);
		exit;	
	}	
	$id = $_POST['id'];
	$num = $_POST['num'];	
	if( !wp_verify_nonce( $_POST['nonce_code'], 'rate-nonce-'. $id ) ) {
		$response['error'] = __('STOP!');
		echo json_encode($response);
		exit;
	}
	$current_user = wp_get_current_user();
	if ( 0 == $current_user->ID ) {
		$response['error'] = __('You must sign in to vote.', 'iv-places');
		echo json_encode($response);
		exit;
	}		
	$rat_array = get_user_meta($current_user->ID, 'iv-user-has-rated', true);
	if( count($rat_array) > 0 && array_key_exists($id, $rat_array) ) {
		$response['error'] = __('Your vote already exists.', 'iv-places');
		echo json_encode($response);
		exit;
	}			
	if( is_array($rat_array) ) {
		$rat_array[$id] = $num;
	} else {
		$rat_array = array($id => $num);
	}
	update_user_meta($current_user->ID, 'iv-user-has-rated', $rat_array);
					
	iv_set_rate('iv-vote-total',$id,(int)iv_get_rate('iv-vote-total',$id) + 1);
	iv_set_rate('iv-vote-rating',$id,(int)iv_get_rate('iv-vote-rating',$id) + $num);
	
	$total = iv_get_rate('iv-vote-total',$id);
	$rating = iv_get_rate('iv-vote-rating',$id);

	if( $total == 0 ) {$total = 1;}
	$response['interest'] = ($rating/($total*5))*100;
	$response['success'] = __('Your vote adding.');
	echo json_encode($response);
	exit;		
}
/**
 *  dysplaing rating
 *  @return array
 */
function iv_rating($p_ID) {
	$disable_class = '';
	if ( !comments_open($p_ID) ) {
		$disable_class = ' disabled';
	}
	$total = iv_get_rate('iv-vote-total', $p_ID);
	$rating = iv_get_rate('iv-vote-rating', $p_ID);

	$total_rec = $total;
	if($total==0) {$total = 1;}
	$pr = ($rating/($total*5))*100;
	$abs = round($rating/$total, 1);
	
	$rate_nonce = wp_create_nonce('rate-nonce-'. $p_ID);
	$output = array();
	$output['abs'] = $abs;
	$ratingHTML = '<ol class="rating show-current"><li>5</li><li>4</li><li>3</li><li>2</li><li>1</li><li class="current"><span style="width:'.$pr.'%"></span></li></ol>';
	$rate_average = '<div class="vote-block" >'. $ratingHTML .'</div>';
	$output['average'] = $rate_average;
	$ratingHTML_2 = '<ol class="rating show-current"><li>5</li><li>4</li><li>3</li><li>2</li><li>1</li><li class="current"><span></span></li></ol> <div class="rating-info" id="rating-info"></div>';
	$rate_bottom = '<div class="vote-block-2'.$disable_class.'" data-id="'. $p_ID .'"  data-rate-nonce="'. $rate_nonce .'" data-total="'. $total_rec .'" data-rating="'. $rating .'">'. $ratingHTML_2 .'</div>';
	$output['bottom'] = $rate_bottom;
	return $output;
}
// translate words for javascript
add_action('wp_footer', 'iv_translate_javascript');
function iv_translate_javascript() {
	?>
	<script type="text/javascript" >
		var tAverage = "<?php _e('Average: ') ?>";
		var tTotal = "<?php _e('Total: ') ?>";
		var tVote = "<?php _e('vote') ?>";
		var tVotes = "<?php _e('votes') ?>";
	</script>
	<?php
}
// set rating
function iv_set_rate($name, $postID, $value) {
    $count_key = $name;
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        update_post_meta($postID, $count_key, $value);
    }
}
// get rating
function iv_get_rate($name, $postID){
    $count_key = $name;
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0";
    }
    return $count.'';
}
