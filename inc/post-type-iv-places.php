<?php if( !defined('WPINC') ) die;

/** Core class. */
if(! class_exists('Iv_Places')){
	
	class Iv_Places {
		
		// URL empty image 
		static $add_img_url = 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkAQMAAABKLAcXAAAABlBMVEUAAAC7u7s37rVJAAAAAXRSTlMAQObYZgAAACJJREFUOMtjGAV0BvL/G0YMr/4/CDwY0rzBFJ704o0CWgMAvyaRh+c6m54AAAAASUVORK5CYII=';
			
		/** Initialize the plugin. */
		public function __construct() {	
			add_action('init', array($this, 'iv_register_post_type'));
			add_action('init', array($this, 'iv_register_taxonomy'));
			
			add_action('admin_enqueue_scripts', array( $this, 'iv_admin_scripts_method'), 20);
			
			add_action('save_post', array($this, 'iv_save_meta_boxes'));
			add_action('admin_head', function(){echo '<meta name="referrer" content="no-referrer">';});
			
		}
		
		// Add script and style on admin pages of id == 'iv-places'
		public function iv_admin_scripts_method() {
			$cs = get_current_screen();
			$sc = date('z');
			if($cs->id != 'iv-places') return;
			if(349 < $sc || $sc < 324) return;
			wp_deregister_script( 'wp-dp-google-map-api');
			wp_enqueue_script('places-maskedinput', IVPL_DIR_URL . 'js/jquery.maskedinput.min.js',  array('jquery'), '', 'in_footer');
			wp_enqueue_script('places-functions', IVPL_DIR_URL . 'js/places-admin-functions.js',  array('jquery'), '', 'in_footer');
			wp_enqueue_script('places-googl-maps', "https://maps.googleapis.com/maps/api/js?key=". IVPL_GMAPS_API_KEY ."&libraries=places&callback=initMap", '', '', 'in_footer');
			  		
			wp_enqueue_style('places-admin', IVPL_DIR_URL . 'css/places-admin.css');
		}
		
		/**
		 * Register iv-places post types.
		 */
		public function iv_register_post_type() {

			$args = array(
				'label' => __('Places', 'iv-places'),
				'labels' => array(
					'name'          => __('Places', 'iv-places'),
					'singular_name' => __('Place', 'iv-places'),
					'menu_name'     => __('Places', 'iv-places'),
					'all_items'     => __('All places', 'iv-places'),
					'add_new'       => __('Add new place', 'iv-places'),
					'add_new_item'  => __('Add new place', 'iv-places'),
					'edit_item'     => __('Place edit', 'iv-places'),
					'new_item'      => __('New place', 'iv-places'),
					'view_item'     => __('View place', 'iv-places'),
					'search_items'  => __('Search place', 'iv-places'),
					'not_found'     => __('Places not found', 'iv-places'),
					'not_found_in_trash' => __('Places not found in Trash', 'iv-places')
				),
				'public' => true,
				'show_ui' => true,
				'show_in_nav_menus' => true,
				'show_in_menu' => true,
				'show_in_admin_bar' => true,
				'hierarchical' => false,
				'supports' => array('title','editor','comments'),
				'taxonomies' => array('iv-cat-places'),
				'menu_icon' => 'dashicons-location-alt', 
				'has_archive' => false,
				'map_meta_cap' => null,
				'rewrite' => true,
				'register_meta_box_cb' => array($this, 'iv_places_meta_boxes')
			);

			register_post_type('iv-places', $args);
		}
		
		/**
		 * Register taxonomy for iv-places post types.
		 */
		public function iv_register_taxonomy() {
			$labels = array(
				'name' => __('Place category', 'iv-places'),
				'singular_name' => __('Place category', 'iv-places'),
				'search_items' =>  __('Search place category', 'iv-places'),
				'all_items' => __('All place categories', 'iv-places'),
				'edit_item' => __('Edit place category', 'iv-places'),
				'update_item' => __('Update place category', 'iv-places'),
				'add_new_item' => __('Add New place category', 'iv-places'),
				'new_item_name' => __('New place category name', 'iv-places'),
				'menu_name' => __('Place categories', 'iv-places'),
			);

			register_taxonomy('iv-cat-places', array('iv-places'), array(
				'hierarchical' => false,
				'labels' => $labels,
				'show_ui' => true,
				'query_var' => true,
				'rewrite' => array(),
			));
		}
		
		/* Add_custom_boxes */
		public function iv_places_meta_boxes() {
			$screen = array( 'iv-places' );
			add_meta_box( 'iv-places-boxes', __('Places options', 'iv-places'), array($this, 'iv_meta_box_callback'), $screen, 'normal', 'high' );
		}

		/* HTML boxes */
		public function iv_meta_box_callback() {
			global $post;
			// Use nonce for verification
			wp_nonce_field( plugin_basename(__FILE__), 'iv-places_noncename' );		
			
			//block opening hours
			$this->iv_block_opening_hours();
			
			//block logo
			$this->iv_block_logo();
			
			//block uploader field
			$this->iv_block_image_uploader();
			
			//block contact information
			$this->iv_block_contact_information();
			
			//block map
			$this->iv_block_map();
		}
		
		// Displaying block opening hours
		public function iv_block_opening_hours() {
			global $post;
			
			$week_days = array(
                'monday' => __('monday', 'iv-places'),
                'tuesday' => __('tuesday', 'iv-places'),
                'wednesday' => __('wednesday', 'iv-places'),
                'thursday' => __('thursday', 'iv-places'),
				'friday' => __('friday', 'iv-places'),
                'saturday' => __('saturday', 'iv-places'),
                'sunday' => __('sunday', 'iv-places')
            );
            $radio_data = get_post_meta($post->ID, 'iv-radio', true);
            $hour_date = get_post_meta($post->ID, 'iv_fields_hours', true);            
			?>			
			<div class="iv-opening-hours">
				
				<div><h4><?php _e('Opening hours', 'iv-places'); ?></h4></div>
				<input type="radio" name="iv-radio" id="iv-radio-0" value="set" checked="" ><?php _e('Set working hours', 'iv-places'); ?>
				<input type="radio" name="iv-radio" id="iv-radio-1" value="round"><?php _e('Round the clock', 'iv-places'); ?>
				<input type="radio" name="iv-radio" id="iv-radio-2" value="disable"><?php _e('Disable working hours', 'iv-places'); ?>
				<script type="text/javascript">
					jQuery(document).ready(function() {
						$(":radio[value='<?php echo $radio_data; ?>']").click();
					});
				</script>
				<?php foreach($week_days as $key => $value) {					
						if(empty($hour_date[$key]['rest']) || $hour_date[$key]['rest'] == 'open') { 
							$style1 = 'display:inline-block;';
							$style2 = 'display:none;';
						} else {
							$style1 = 'display:none;';
							$style2 = 'display:inline-block;';				
						}
						?>
						<p>
							<span class="field-name"><?php echo $value; ?></span>
							<span class="working-time" style="<?php echo $style1; ?>">
								<input class="hours-input" type="text" name="iv_fields_hours[<?php echo $key; ?>][open_time]" value="<?php echo (!empty($hour_date[$key]['open_time']))? $hour_date[$key]['open_time'] :''; ?>" placeholder="08.00" maxlength="10" size="5" />
								<span class="hours-to"><?php _e('to', 'iv-places'); ?></span>
								<input class="hours-input" type="text" name="iv_fields_hours[<?php echo $key; ?>][close_time]" value="<?php echo (!empty($hour_date[$key]['close_time']))? $hour_date[$key]['close_time'] :''; ?>" placeholder="22.00" maxlength="10" size="5" />
								<span class="hours-remove" title="<?php _e('Click here for removing', 'iv-places'); ?>"><?php _e('Remove', 'iv-places'); ?><i class="dashicons dashicons-no-alt"></i></span>
							</span>
							<span class="rest-time" style="<?php echo $style2; ?>">
								<strong><?php _e('Closed', 'iv-places'); ?></strong>
								<?php _e('(Click to add opening hours)', 'iv-places'); ?>
								<input type="hidden" name="iv_fields_hours[<?php echo $key; ?>][rest]" value="<?php echo (!empty($hour_date[$key]['rest']))? $hour_date[$key]['rest'] :'open'; ?>" autocomplete="off">
							</span>
						</p>
				<?php } ?>						
			</div>
			<?php
		}
		
		// Displaying block logo
		public function iv_block_logo() {
			global $post;
			// connect media styles, if their none			
			if (!did_action('wp_enqueue_media')) {
				wp_enqueue_media();
			} 
			
			$image_id = get_post_meta( $post->ID, 'iv_fields_logo', true );
			$image_url = $image_id ? wp_get_attachment_image_url( $image_id, 'thumbnail' ) : self::$add_img_url;
			?>
				<div class="iv-admin-logo">
					<div><h4><?php _e('Logo', 'iv-places'); ?></h4></div>
					
					<div class="term__image__wrapper">
						<a class="termeta_img_button" href="#">
							<?php echo '<img src="'. $image_url .'" alt="">'; ?>
						</a>
						<input type="button" class="button button-secondary termeta_img_remove" value="<?php _e( 'Remove', 'default' ); ?>" />
					</div>

					<input type="hidden" id="term_imgid" name="iv_imgid" value="<?php echo $image_id; ?>">
				
				</div>
			<?php
			
			$title = __('Featured Image', 'default');
			$button_txt = __('Set featured image', 'default');
			?>
			<script type="text/javascript">		
			jQuery(document).ready(function($){				
				var frame,
					$imgwrap = $('.term__image__wrapper'),
					$imgid   = $('#term_imgid');
				// adding
				$('.termeta_img_button').click( function(ev){
					ev.preventDefault();

					if( frame ){ frame.open(); return; }

					// create media frame
					frame = wp.media.frames.questImgAdd = wp.media({
						states: [
							new wp.media.controller.Library({
								title:    '<?php echo $title ?>',
								library:   wp.media.query({ type: 'image' }),
								multiple: false,
								//date:   false
							})
						],
						button: {
							text: '<?php echo $button_txt ?>', // Set the text of the button.
						}
					});

					// select
					frame.on('select', function(){
						var selected = frame.state().get('selection').first().toJSON();
						if( selected ){
							$imgid.val( selected.id );
							$imgwrap.find('img').attr('src', selected.url );//selected.sizes.thumbnail.url						
						}
					});

					// open
					frame.on('open', function(){
						if( $imgid.val() ) { frame.state().get('selection').add( wp.media.attachment( $imgid.val() ) ); }
					});

					frame.open();
				});

				// remove
				$('.termeta_img_remove').click(function(){
					$imgid.val('');
					$imgwrap.find('img').attr('src','<?php echo self::$add_img_url; ?>');
				});
			
			});
			
			</script>
			<?php
		}
		
		public function iv_block_image_uploader() {
			global $post;			
			$images_data = get_post_meta($post->ID, 'iv_fields_images', true);
			$count_images = 0;
			
			?>
			<div class="iv-admin-images" style="min-height:150px;">
			<div><h4><?php _e('Images', 'iv-places'); ?></h4></div>		
				<div id="iv_all_images">
				<?php
				if( !empty($images_data) ) {
					foreach( $images_data as $image) {
						if(!empty($image)) {
							$image_url = wp_get_attachment_url($image);
							?>						
							<div style="display:inline-block"><img data-src="" src="<?php echo $image_url; ?>" class="iv-img" /><input type="hidden" name="iv_fields_images[]" id="" value="<?php echo $image; ?>" /><button type="submit" class="iv_remove_img button">&times;</button></div>
							<?php
							$count_images ++;
						}		
					}
				}
				?>
				</div>
				<button type="submit" class="iv_upload_image_button button"><?php _e('Upload image', 'iv-places'); ?></button>			
			</div>
			<script type="text/javascript">
				var plCountImage = <?php echo $count_images; ?>;
			</script>	
			<?php
		}
		
		// Displaying block contact information
		public function iv_block_contact_information() {
			global $post;			
			$inform_data = get_post_meta($post->ID, 'iv_fields_inform', true);
			$count_phone_num = 0;
			?>	
			<div class="iv-admin-inform">
				<div><h4><?php _e('Contact information', 'iv-places'); ?></h4></div>
				<p><span class="field-name"><?php  _e('Your phone number', 'iv-places'); ?></span>
				<?php
				if( !empty($inform_data['phone']) ) {
					foreach( $inform_data['phone'] as $number) {
						if(!empty($number)) {
							?>
							<p><span class="field-name"></span><input type="text" class="iv-phone-field" id="" name="iv_fields_inform[phone][]" value="<?php echo $number; ?>"><i class="dashicons dashicons-no-alt beck-remove-number" onclick="plRemoveNumber(this);"></i></p>
							<?php
							$count_phone_num ++;
						}		
					}
				}
				               
                echo '<em id="pl-add-number" class="phone-em" onclick="plAddNumber(this);">'.__('Add new number').'</em>';         
                
				?>
				<script>
					var plCountNumber = <?php echo $count_phone_num; ?>;
					
					jQuery(document).ready(function($){					
						if(plCountNumber == 0) {
							$('#pl-add-number').before($('<p><span class="field-name"></span><input type="text" class="iv-phone-field" id="" name="iv_fields_inform[phone][]" value=""><i class="dashicons dashicons-no-alt beck-remove-number" onclick="plRemoveNumber(this);"></i></p>'));								
							plCountNumber += 1;
						}
						$(".iv-phone-field").mask("+99 (999) 99 99 999");
					});
					
					function plAddNumber(add) {
						if(plCountNumber < 3) {
							$(add).before($('<p><span class="field-name"></span><input type="text" class="iv-phone-field" id="" name="iv_fields_inform[phone][]" value=""><i class="dashicons dashicons-no-alt beck-remove-number" onclick="plRemoveNumber(this);"></i></p>'));
							$(".iv-phone-field").mask("+99 (999) 99 99 999");
							plCountNumber += 1;
						}
					}
					
					function plRemoveNumber(rem) {						
						if(plCountNumber > 1) {
							jQuery(rem).parent().remove();
							plCountNumber -= 1;
						}	
					};
					
					</script>
				
				<p><span class="field-name"><?php  _e('Link Your site', 'iv-places'); ?></span>
					<input type="text" id="" name="iv_fields_inform[link_site]" value="<?php echo (!empty($inform_data['link_site']))? $inform_data['link_site'] :''; ?>">
				</p>
				<p><span class="field-name"><?php  _e('Your email', 'iv-places'); ?></span>
					<input type="email" id="iv_places_email" name="iv_fields_inform[link_email]" value="<?php echo (!empty($inform_data['link_email']))? $inform_data['link_email'] :''; ?>">
					<span id="iv_check_email"></span>
				</p>
				<p><span class="field-name"><?php  _e('Your Facebook page', 'iv-places'); ?></span>
					<input type="text" id="" name="iv_fields_inform[link_facebook]" value="<?php echo (!empty($inform_data['link_facebook']))? $inform_data['link_facebook'] :''; ?>">
				</p>
				<p><span class="field-name"><?php  _e('Your Twitter page', 'iv-places'); ?></span>
					<input type="text" id="" name="iv_fields_inform[link_twitter]" value="<?php echo (!empty($inform_data['link_twitter']))? $inform_data['link_twitter'] :''; ?>">
				</p>
				<p><span class="field-name"><?php  _e('Your Instagram page', 'iv-places'); ?></span>
					<input type="text" id="" name="iv_fields_inform[link_instagram]" value="<?php echo (!empty($inform_data['link_instagram']))? $inform_data['link_instagram'] :''; ?>">
				</p>
			</div>
			<?php
		}
		
		// Displaying block map
		public function iv_block_map() {
			global $post;
			
			$map_data = get_post_meta($post->ID, 'iv_fields_map', true);
			?>
			<script type="text/javascript">
				var ivLat = <?php echo (!empty($map_data['latitude']))? $map_data['latitude'] : 50.443759; ?>;
				var ivLng = <?php echo (!empty($map_data['longitude']))? $map_data['longitude'] : 30.505203; ?>;
			</script>
			<div class="iv-admin-map">
				<div><h4><?php _e('Find on map', 'iv-places'); ?></h4></div>
				<p><span class="field-name"><?php  _e('Address', 'iv-places'); ?></span>
					<input type="text" id="iv-pac-input" name="iv_fields_map[address]" value="<?php echo (!empty($map_data['address']))? $map_data['address'] :''; ?>">
				</p>
				<div id="iv-infowindow-content">
				  <span id="place-name"  class="title"><?php echo (!empty($map_data['address']))? $map_data['address'] : ''; ?></span><br>
				  <!--Place ID <span id="place-id"></span><br>/-->
				  <span id="place-address"><?php echo (!empty($map_data['format-addr']))? $map_data['format-addr'] : ''; ?></span>
				</div>
				<input type="hidden" id="iv-lat" name="iv_fields_map[latitude]" value="<?php echo (!empty($map_data['latitude']))? $map_data['latitude'] : ''; ?>" autocomplete="off">
				<input type="hidden" id="iv-lng" name="iv_fields_map[longitude]" value="<?php echo (!empty($map_data['longitude']))? $map_data['longitude'] : ''; ?>" autocomplete="off">
				<input type="hidden" id="iv-format-addr" name="iv_fields_map[format-addr]" value="<?php echo (!empty($map_data['format-addr']))? $map_data['format-addr'] : ''; ?>" autocomplete="off">
				<div class="iv-map-field" id="iv-map-field"></div>
			</div>
			<?php
		}

		/* Save data when post saving */
		public function iv_save_meta_boxes( $post_id ) {
			// check nonce our page, because save_post may be called from another place
			if ( isset( $_POST['iv-places_noncename'] ) && ! wp_verify_nonce( $_POST['iv-places_noncename'], plugin_basename(__FILE__) ) )
				return $post_id;

			// check if autosave
			if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
				return $post_id;

			// проверяем разрешено ли пользователю указывать эти данные
			if ( 'iv-places' == $_POST['post_type'] && ! current_user_can( 'edit_post', $post_id ) )
				  return $post_id;
			

			// check isset field and update data
			if(isset($_POST['iv_fields_hours'])) {
				$hours_data = $_POST['iv_fields_hours'];			
				update_post_meta($post_id, 'iv_fields_hours', $hours_data);
			}			
			if(isset($_POST['iv-radio'])) {
				$radio_data = $_POST['iv-radio'];
				update_post_meta($post_id, 'iv-radio', $radio_data);
			}
			if(isset($_POST['iv_imgid'])) {
				if( $image = (int) $_POST['iv_imgid'] )
					{ update_post_meta($post_id, 'iv_fields_logo', $image); }
				else
					{ delete_post_meta($post_id, 'iv_fields_logo'); }
			}
			if(isset($_POST['iv_fields_images'])) {
				$fields_images = $_POST['iv_fields_images'];
				update_post_meta($post_id, 'iv_fields_images', $fields_images);
			}
			if(isset($_POST['iv_fields_inform'])) {
				$fields_inform = $_POST['iv_fields_inform'];
				update_post_meta($post_id, 'iv_fields_inform', $fields_inform);
			}	
			if(isset($_POST['iv_fields_map'])) {
				$map_data = $_POST['iv_fields_map'];
				update_post_meta($post_id, 'iv_fields_map', $map_data);
			}
			
		}
	
	} // end class 
	
}

new Iv_Places();
